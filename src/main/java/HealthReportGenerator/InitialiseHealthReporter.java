package HealthReportGenerator;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * <h1>The InitialiseHealthReporter class is responsible for setting the applications initial state.</h1>
 * <p>
 * The InitialiseHealthReporter class will perform the following actions;
 * <ul style=“list-style-type:square”>
 *     <li>Setting the initial configuration data.</li>
 *     <li>Instantiating a new report.docx.</li>
 *     <li>Iterating the given list of Mediaflux Health Check log files & performing the report and summary data construction.</li>
 * </ul>
 *
 * @author  Jayson Brenton
 */
public class InitialiseHealthReporter {
    /**
     * use the classname for the logger
     */
    private final static Logger _LOGGER = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );

    /**
     * Application entry point.
     * */
    public InitialiseHealthReporter(){
        try {
            AppLogger.setup();
        }
        catch ( IOException e ) {
            e.printStackTrace();
            throw new RuntimeException("Problems with creating the log files");
        }

        _LOGGER.setLevel( AppLogger._configLogLevel );

        start();

    }

    /**
     * Begin processing the logs found in the defined logs directory as set in HealthReporter.xml
     * */
    private static void start(){
        /**
         * Get the configuration objects.
         * */
        Map<String, String> globalConfigData = new ConfigurationData().globalConfigData();
        ServerReport serverReport = new ServerReport();
        String currentLogFilePath;

        _LOGGER.info("Start Health Report Creation");

        /**
         * globalConfigData contains properties that are common to the application
         * */
        if( globalConfigData == null ){
            _LOGGER.severe("Unable to set the global configuration data. Please ensure the HealthReportGenerator.xml configuration file exists.");
            System.exit(-1);

        }
        else if( globalConfigData.size() == 0 ){
            _LOGGER.severe( "Unable to set the global configuration data. Please ensure the HealthReportGenerator.xml configuration file exists." );
            System.exit(-1 );

        }
        else{
            /**
             * Get the path to the log directory
             * */
            String logDir = globalConfigData.get( "logs.directory.path" );
            if( !Utilities.testDirectoryExists( logDir ) ){
                _LOGGER.severe( "Unable to set the Mediaflux log directory path." );
                System.exit(-1);
            }

            /**
             * Get a list of all the log files
             * Note: No need to test the logFileList for null or size as this has been checked above with Utilities.validateLogsDirectory( logDir )
             * */
            List<String> logFileList = Utilities.getLogFileList( logDir );
            if( logFileList == null){
                _LOGGER.severe( "Unable to retrieve the Mediaflux log files." );
                System.exit(-1 );
            }

            /**
             * Create word document
             * */
            serverReport.initialiseReportDocument();

            /**
             * Process each of the log files
             * */
            for ( int logFileCounter = 0; logFileCounter < logFileList.size(); logFileCounter++ ){

                /**
                 * Get the path to the currently processed log file.
                 * Note: we know the log file exists as it was only valid log files will be in the logFileList
                 * */
                currentLogFilePath = logFileList.get( logFileCounter );
                _LOGGER.info( "Processing Log File: " + currentLogFilePath );

                /**
                 * Perform the creation of the Word report.
                 * */
                serverReport.buildReportDocument( currentLogFilePath );

            }

            /**
             * Performs validation of the report and updates to the summary table if required.
             * */
            serverReport.updateSummaryTable();

            /**
             * Saves the Word report.
             * */
            serverReport.saveReportDocument();

        }
    }
}
