package HealthReportGenerator;

import org.apache.commons.io.FilenameUtils;
import java.io.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>The Utilities class provides access to commonly used methods.</h1>
 * <p>
 * The Utilities class provides access to commonly used methods.
 *
 * @author  Jayson Brenton
 */
public class Utilities {
    /**
     * Returns the result of a data test.
     * */
    public boolean testData( String[] data ) {
        final boolean b = setTestData(data);
        return b;

    }

    /**
     * Sets the result of a data test.
     * */
    private boolean setTestData( String[] data ) {
        String[] testData = data;
        return testDataExists( testData );

    }

    /**
     * Returns the result of a template exists test.
     * */
    public boolean validateTemplate( String path ) {
        final boolean b = setValidateTemplate(path);
        return b;

    }

    /**
     * Sets the result of a template exists test.
     * */
    private boolean setValidateTemplate( String path ) {
        final boolean b = validatePathToTemplate(path);
        return b;

    }

    /**
     * Returns the uuid.
     * */
    public String uniqueIDS( String id ) {
        final String s = setUniqueIDS( id );
        return s;

    }

    /**
     * Sets the uuid.
     * */
    private String setUniqueIDS( String id ) {
        final String uniqueIDS = getUniqueMFIdentifiers( id );
        return uniqueIDS;

    }

    /**
     * Returns the current date.
     * */
    public String currentDate() {
        final String s = setCurrentDate( );
        return s;

    }

    /**
     * Sets the current date.
     * */
    private String setCurrentDate() {
        final String currentDate = getCurrentDate( );
        return currentDate;

    }

    /**
     * Returns formatted date.
     * */
    public String formatDate() {
        final String s = setFormatDate( );
        return s;

    }

    /**
     * Sets the formatted date.
     * */
    private String setFormatDate() {
        final String formattedDate = dateFormatter( );
        return formattedDate;

    }

    /**
     * Sets the log row number that will be used as the start point for the current log data.
     * */
    public Map<String, Integer> rowCount( Map<String, String> logDataMap ) {
        final Map<String, Integer> m = setRowCount( logDataMap );
        return m;

    }

    /**
     * Returns the log row number.
     * */
    private Map<String, Integer> setRowCount( Map<String, String> logDataMap ) {
        final Map<String, Integer> formattedDate = getRowCount( logDataMap );
        return formattedDate;

    }

    /**
     * Extract text between two strings
     * */
    public String textExtraction( String pattern, String textToSearch ) {
        final String s = setTextExtraction( pattern, textToSearch);
        return s;

    }

    /**
     * Sets the result of a data extract.
     * */
    private String setTextExtraction( String pattern, String textToSearch ) {
        String p = pattern;
        String tts = textToSearch;
        final String s = extractText( p, tts);
        return s;

    }

    /**
     * Test data existence.
     *
     * @return*/
    public Boolean testDataExistence( String[] data ) {
        final Boolean aBoolean = setTextDataExistence(data);
        return aBoolean;

    }

    /**
     * Sets the result of a data existence test.
     *
     * @return*/
    private Boolean setTextDataExistence( String[] data ) {
        final Boolean aBoolean = testDataExists(data);
        return aBoolean;

    }

    /**
     * Test data existence.
     *
     * @return*/
    public double percentageCalculator( String maxVal, String minVal ) {
        final double v = setPercentageCalculator( maxVal, minVal);
        return v;

    }

    /**
     * Sets the result of a data existence test.
     *
     * @return*/
    private double setPercentageCalculator( String maxVal, String minVal ) {
        final double v = calculatePercentDiff( maxVal, minVal);
        return v;

    }

    // Class methods
    /**
     * Validates that we have the required config and log data values.
     * Returns true if data supplied and false if any data element is not supplied.
     * */
    private static Boolean testDataExists( String[] data ){
        Boolean inputValidationResult = true;

        if( data == null){
            return false;

        }
        else{
            for ( int dataCounter = 0; dataCounter < data.length; dataCounter++ ){
                String d = data[ dataCounter ];

                if( d == null ){
                    inputValidationResult = false;

                }
                else if( d.equals( "" ) ){
                    inputValidationResult = false;

                }
            }
        }

        return inputValidationResult;

    }

    /**
     * Returns the text matching the supplied regex.
     * */
    private static String extractText( String pattern, String textToSearch ) {
        String compPattern = pattern;
        Pattern p = Pattern.compile( compPattern, Pattern.DOTALL);
        Matcher m = p.matcher( textToSearch );
        m.find();

        try {
            String res = m.group(0).trim();
            return res;

        }
        catch ( java.lang.IllegalStateException e ) {
            return null;
        }
    }

    /**
     * Test if string is numeric
     * */
    public static boolean isNumeric( String s) {
        if( s == null || s.isEmpty() ){
            return false;

        }
        else if( s.matches(".*[a-zA-Z].*") ){
            return false;

        }
        else if(s.matches("^[0-9]+([,.]\\d+)?$")){
            return true;

        }
        else{
            return false;
        }
    }

    /**
     * Simple date formatter
     * */
    private static String dateFormatter( ){
        String pattern = "dd-MMM-YYYY";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        return date;
    }

    /**
     * Converts a string to a double
     * */
    public static double getDouble( String s ){
        double res = -1;
        Number number;

        if( s == null || s.isEmpty() ){
            return res;
        }

        if( isNumeric( s ) ){
            try {
                number = NumberFormat.getInstance().parse(s);
                return number.doubleValue();

            }
            catch (ParseException e) {
                e.printStackTrace();
                return res;
            }
        }

        return res;
    }

    /**
     * Converts a string to a long
     * */
    public static long getLong( String s ){
        long res = -1;
        Number number;

        if( s == null || s.isEmpty() ){
            return res;
        }

        if(isNumeric( s )){
            try {
                number = NumberFormat.getInstance().parse( s );
                return number.longValue();

            }
            catch (ParseException e) {
                e.printStackTrace();
                return res;
            }
        }

        return res;
    }

    /**
     * Calculates the percentage difference of two numbers. Used for the RAM warning checks
     * */
    private static double calculatePercentDiff( String maxVal, String minVal ){
        double min = Utilities.getLong( minVal );
        double max = Utilities.getLong( maxVal );
        double res = ( double ) ( min / max ) * 100;
        return res;
    }

    /**
     * Returns an array list of all the logs files.
     * */
    public static List<String> getLogFileList( String logDirectoryPath ){
        List<String> arrLst = new ArrayList<>();

        try{
            for ( Path path : Files.newDirectoryStream( Paths.get( logDirectoryPath ) ) ){
                path = path.normalize();
                String fullLogFilePath = path.toAbsolutePath().toString();
                String fileType = FilenameUtils.getExtension( fullLogFilePath ).toLowerCase();

                if( fileType.equals( "log" ) ){
                    arrLst.add( fullLogFilePath );

                }
            }
        }
        catch( IOException e ){
            return null;

        }

        return arrLst;

    }

    /**
     * Get the current date.
     * */
    private static String getCurrentDate( ){
        String datePattern = "yyyyMMdd";
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern( datePattern );
        return now.format( formatter );
    }

    /**
     * Build the table row counts for job, network, package and store sub table creation.
     * Captures the following,
     *  Number of sub table rows required for store data.
     *  Number of sub table rows required for job data.
     * */
    private static Map<String, Integer> getRowCount( Map<String, String> logDataMap ) {
        Map<String, Integer> map = new HashMap<>();
        int jobSubTableRowCounter = 0;
        int storeSubTableRowCounter = 0;
        int svcSubTableRowCounter = 0;
        int pacSubTableRowCounter = 0;

        /**
         * Each store/job/package/network entry in logDataMap will correspond to a new row in the sub table
         * */
        for ( Map.Entry<String, String> logDataEntry : logDataMap.entrySet() ) {
            String logDataValue = logDataEntry.getValue();

            if( logDataValue.contains( "Store name" ) ){
                storeSubTableRowCounter++;

            }else if( logDataValue.contains( "Job name" ) ){
                jobSubTableRowCounter++;

            }else if( logDataValue.contains( "Service port" ) ){
                svcSubTableRowCounter++;

            }else if( logDataValue.contains( "Package name" ) ){
                pacSubTableRowCounter++;

            }
        }

        /**
         * Return the row counts for each multi element data set
         * */
        map.put( "job", jobSubTableRowCounter );
        map.put( "store", storeSubTableRowCounter );
        map.put( "service", svcSubTableRowCounter );
        map.put( "package", pacSubTableRowCounter );

        return map;

    }

    /**
     * Validates path to the word template file. Acceptable input files will be,
     *  doc, docx
     * */
    private static boolean validatePathToTemplate( String filePath ){

        if( filePath == null || filePath == "" ){
            return false;

        }

        String fileType = FilenameUtils.getExtension( filePath ).toLowerCase();
        File file = new File( filePath );

        if( fileType.equals( "docx" ) || fileType.equals( "doc" ) ){
            /**
             * Correct extensions. Validate that the file exists.
             * */
            if ( file.exists() ) {
                return true;

            }
            else{
                return false;
            }

        }
        else{
            /**
             * Template file must be either a doc or docx
             * */
            return false;

        }
    }

    /**
     * Validates that the provided directory path is a valid directory. Returns false if not a directory.
     * */
    public static boolean testDirectoryExists( String directoryPath ){
        File dir = new File( directoryPath );

        if( dir.isDirectory() ){
            return true;

        }else{
            System.out.println( "Error. Invalid directory path. Provided: " + directoryPath );
            return false;

        }
    }

    // Return the host name / uuid from a word table.
    private static String getUniqueMFIdentifiers( String text ){
        String id = "";

        int lenCell = text.length();
        int indexStart = text.indexOf( ":" ) + 1;
        id = text.substring( indexStart, lenCell ).trim();

        return id;
    }

}
