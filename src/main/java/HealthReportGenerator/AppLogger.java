package HealthReportGenerator;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.logging.*;
//import static java.lang.System.currentTimeMillis;

public class AppLogger {
    public static Level _configLogLevel;
    private static final Map<String, String> _globalConfigData = new ConfigurationData().globalConfigData();

    static public void setup() throws IOException {
        /**
         * Get the global logger to configure it
         * */
        Logger logger = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );

        /**
         * Set the _configLogLevel to the configurations level.
         * */
        setLoggerLevel();
        logger.setLevel( _configLogLevel );

        /**
         * Set the file handler
         * */
        String applicationLogFilePath = getLoggerDirectory();
        FileHandler fileTxt = new FileHandler(applicationLogFilePath + "healthReporter.log" );

        /*final long start = currentTimeMillis();
        fileTxt.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return String.format("%1$tc %2$s%n%4$s: %5$s%6$s%n", record.getMillis() - start, record.getMessage());
            }
        });*/

        /**
         * Set the console handler
         * */
        //Handler consoleHandler = new ConsoleHandler();
        //consoleHandler.setLevel( _configLogLevel );

        /**
         * Create a TXT formatter
         * */
        SimpleFormatter formatterTxt = new SimpleFormatter();
        fileTxt.setFormatter( formatterTxt );
        //consoleHandler.setFormatter( formatterTxt );

        logger.addHandler( fileTxt ) ;
        //logger.addHandler( consoleHandler );
    }

    /**
     * Set the Logger _configLogLevel to the configurations level.
     * */
    private static void setLoggerLevel(){
        /**
         * Get the application config settings.
         * */
        Map<String, String> globalConfigData = new ConfigurationData().globalConfigData();

        /**
         * Get the application log level and ensure provided level is acceptable.
         * */
        String configurationLogLevel = globalConfigData.get( "application.log.level" ).toUpperCase(Locale.ROOT);

        /**
         * Test the provided configuration log level setting is within the accepted levels list.
         * */
        String acceptedLevels = String.valueOf( Arrays.asList( "SEVERE","WARNING", "INFO", "CONFIG", "FINE", "FINER", "FINEST" ) );
        if( configurationLogLevel == null || configurationLogLevel == "" ){
            System.out.println( "Exception. Please provide a valid application.log.level. Provided: " + configurationLogLevel );
            System.exit(-1);
        }
        else if( !acceptedLevels.contains( configurationLogLevel ) ){
            System.out.println( "Exception. Please provide a valid application.log.level. Provided: " + configurationLogLevel );
            System.exit(-1);
        }

        /**
         * Set the logger level to the configurations current level.
         * */
        if( configurationLogLevel.toUpperCase(Locale.ROOT).equals( "SEVERE" ) ){
            _configLogLevel = Level.SEVERE;
        }
        else if( configurationLogLevel.toUpperCase(Locale.ROOT).equals( "WARNING" ) ){
            _configLogLevel = Level.WARNING;
        }
        else if( configurationLogLevel.toUpperCase(Locale.ROOT).equals( "INFO" ) ){
            _configLogLevel = Level.INFO;
        }
        else if( configurationLogLevel.toUpperCase(Locale.ROOT).equals( "CONFIG" ) ){
            _configLogLevel = Level.CONFIG;
        }
        else if( configurationLogLevel.toUpperCase(Locale.ROOT).equals( "FINE" ) ){
            _configLogLevel = Level.FINE;
        }
        else if( configurationLogLevel.toUpperCase(Locale.ROOT).equals( "FINER" ) ){
            _configLogLevel = Level.FINER;
        }
        else if( configurationLogLevel.toUpperCase(Locale.ROOT).equals( "FINEST" ) ){
            _configLogLevel = Level.FINEST;
        }
    }

    /**
     * Get and check the path to the application log directory.
     * */
    private static String getLoggerDirectory(){
        String applicationLogFilePath;

        applicationLogFilePath = _globalConfigData.get( "application.log.directory" );
        if( !Utilities.testDirectoryExists( applicationLogFilePath ) ){
            System.out.println( "Exception. Please provide a valid application log directory. Provided: " + applicationLogFilePath );
            System.exit(-1);
        }

        return applicationLogFilePath;
    }
}
