package HealthReportGenerator;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * <h1>The LogData class is responsible for the construction of the Mediaflux server log objects.</h1>
 * <p>
 * The LogData class will perform the construction of the following configuration data sets;
 * <ul style=“list-style-type:square”>
 *     <li>The construction of the Mediaflux servers log data objects.</li>
 * </ul>
 *
 * @author  Jayson Brenton
 */
public class LogData {
    private Map<String, String> _AllServerLogData = new HashMap<>();
    private Map<String, String> _singleElementLogData = new HashMap<>();
    private Map<String, String> _multiElementLogData = new HashMap<>();

    private final static Logger _LOGGER = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private static Utilities _utils = null;

    /**
     * Returns the most recent log data for a given log file
     * */
    public Map<String, String> serverAllLogData() {
        return _AllServerLogData;

    }

    /**
     * Sets the most recent log data for a given log file
     * */
    private void setServerAllLogData( String logFilePath ) {
        _AllServerLogData = getAllLogData( logFilePath );

    }

    /**
     * Returns the log data for lines that contain single element data.
     * For example, number of CPU's.
     * Ignore multi-element log line which are those that contain multiple log elements.
     * For example, job, store, network and package data
     * */
    public Map<String, String> singleElementServerLogData() {
        return _singleElementLogData;

    }

    /**
     * Sets the log data for lines that contain single element data.
     * */
    private void setSingleElementServerLogData( Map<String, String> primaryLogData ) {
        _singleElementLogData = setMainLogDataKeys( primaryLogData );

    }

    /**
     * Returns the log data for lines that contain multi-element data.
     * For example, job, store, network and package data
     * Ignore multi-element log line which are those that contain multiple log elements.
     * For example, number of CPU's.
     * */
    public Map<String, String> multiElementServerLogData() {
        return _multiElementLogData;

    }

    /**
     * Sets the log data for lines that contain multi-element data.
     * */
    private void setMultiElementServerLogData( Map<String, String> subLogData ) {
        _multiElementLogData = getSubLogData( subLogData );

    }

    /**
     * Constructor
     * */
    public LogData( String logFilePath, Map<String, String> logDataMap ){
        _LOGGER.setLevel( AppLogger._configLogLevel );
        _utils = new Utilities();

        if( logFilePath != null && logDataMap == null ){
            setServerAllLogData( logFilePath );

        }
        else if( logFilePath == null ){
            setSingleElementServerLogData( logDataMap );
            setMultiElementServerLogData( logDataMap );

        }
    }

    /**
     * Returns the latest set of log file data for a given lof file.
     * */
    private static Map<String, String> getAllLogData( String logFilePath ) {
        Map<String, String> map = new HashMap<>();
        File log = new File( logFilePath );
        String line;
        int lineCounter = 1;

        /**
         * Put the current log file path in the map.
         * */
        map.put( "logFilePath", logFilePath);

        try{
            /**
             * Finds the line number of the last reference to "Mediaflux Server Name" in the log file
             * So we only process the most recent log entries
             * */
            int readFromLineNumber = getLineIndex( logFilePath, "Executing Daily Health Check Process" );
            int storeCounter = 1;
            int jobCounter = 1;
            int svcCounter = 1;
            int packCounter = 1;

            String uuid = getUUID( logFilePath, readFromLineNumber );

            FileReader fr = new FileReader( log );
            BufferedReader br = new BufferedReader( fr );

            if( uuid != null ){
                while (( line = br.readLine() ) != null) {
                    line = line.trim();
                    String param = null;
                    String value = null;

                    if ( lineCounter >= readFromLineNumber & !line.contains( "Heading" ) ) {
                        if( line.contains( "Job name" ) ){
                            value = "Job name: " + _utils.textExtraction("(?<=Job name:)(.*)(?=)", line );
                            param = "UUID " + uuid + " Job " + jobCounter;
                            jobCounter++;

                        }else if( line.contains( "Store name" ) ){
                            value = "Store name: " + _utils.textExtraction("(?<=Store name: )(.*)(?=)", line );
                            param = "UUID " + uuid + " Store " + storeCounter;
                            storeCounter++;

                        }else if( line.contains( "Service port" ) ){
                            value = "Service port: " + _utils.textExtraction("(?<=Service port:)(.*)(?=)", line );
                            param = "UUID " + uuid + " Service " + svcCounter;
                            svcCounter++;

                        }else if( line.contains( "Package name" ) ){
                            value = "Package name: " + _utils.textExtraction("(?<=Package name:)(.*)(?=)", line );
                            param = "UUID " + uuid + " Package " + packCounter;
                            packCounter++;

                        }else{
                            if( !line.contains("Number of Assets")){
                                param = _utils.textExtraction("(?<=Parameter: ')(.*)(?=' Value)", line );
                                value = _utils.textExtraction("(?<=Value: ')(.*)(?=')", line );

                            }
                        }
                        map.put( param, value );

                        _LOGGER.finest("Extracting log data. Name: " + param + ". Value: " + value );
                    }
                    lineCounter++;

                }

            }else{
                return null;

            }

            fr.close();

        } catch ( IOException e ) {
            _LOGGER.severe( e.getMessage() );

            return null;

        }

        return map;
    }

    /**
     * Finds the line number of the last occurrence of a string
     * */
    private static int getLineIndex( String logFilePath, String searchString) throws FileNotFoundException {
        int res = 0;

        Scanner scanner = new Scanner( new File( logFilePath ) );
        int lineIndex = 0;

        Scanner sc = new Scanner( searchString );
        String nextline = sc.nextLine();

        while (scanner.hasNextLine()) {
            final String lineFromFile = scanner.nextLine();
            lineIndex++;

            if ( lineFromFile.contains( nextline ) ) {
                res = lineIndex;
            }
        }

        _LOGGER.finest("Reading from line " + res );

        return res;
    }

    /**
     * Replaces the key names in the log data map.
     * */
    private Map<String, String> setMainLogDataKeys( Map<String, String> logDataMap ){
        Map<String, String> newMap = new HashMap<>();
        String uuid = logDataMap.get( "MF uuid" );

        newMap.put( "mfServerIdentifier", "UUID: " + uuid + ". " + logDataMap.get( "Mediaflux Server Name" ) );
        newMap.put( "mfVersion", logDataMap.get( "Version" ) );
        newMap.put( "mfUUID", logDataMap.get( "MF uuid" ) );
        newMap.put( "mfBuildTime", logDataMap.get( "MF Build Time" ) );
        newMap.put( "mfServerType", logDataMap.get( "Server type" ) );
        newMap.put( "mfCurrentTime", logDataMap.get( "Current Time" ) );
        newMap.put( "mfStartTime", logDataMap.get( "Start Time" ) );
        newMap.put( "mfServerUpTime", logDataMap.get( "Up Time" ) );
        newMap.put( "numCPUs", logDataMap.get( "Num CPUs" ) );
        newMap.put( "mfServerAvgLoad", logDataMap.get( "Average Load" ) );
        newMap.put( "mfNumOpenStreams", logDataMap.get( "Open Streams" ) );
        newMap.put( "mfRepStart", logDataMap.get( "Replication Start Time" ) );
        newMap.put( "mfRepEnd", logDataMap.get( "Replication End Time" ) );
        newMap.put( "mfRepDuration", logDataMap.get( "Replication Duration" ) );
        newMap.put( "mfRepFailed", logDataMap.get( "Replication Failed" ) );
        newMap.put( "osVersion", logDataMap.get( "Mediaflux OS Version" ) );
        newMap.put( "osName", logDataMap.get( "Mediaflux OS Name" ) );
        newMap.put( "osArch", logDataMap.get( "Mediaflux OS Architecture" ) );
        newMap.put( "mfRAMTotal", logDataMap.get( "Assigned Memory" ) );
        newMap.put( "mfRAMUsed", logDataMap.get( "Used Memory" ) );
        newMap.put( "mfRAMFree", logDataMap.get( "Free Memory" ) );
        newMap.put( "mfRAMTotalBytes", logDataMap.get( "Total Memory Bytes" ) );
        newMap.put( "mfRAMUsedBytes", logDataMap.get( "Used Memory Bytes" ) );
        newMap.put( "mfRAMFreeBytes", logDataMap.get( "Free Memory Bytes" ) );
        newMap.put( "mfRAMMaxBytes", logDataMap.get( "Max Memory Bytes" ) );

        return newMap;

    }

    /**
     * Returns a map of the log data for jobs, stores,  packages  and  network services.
     * */
    private Map<String, String> getSubLogData( Map<String, String> currentLogData ){
        Map<String, String> map = new HashMap<>();

        for ( Map.Entry<String, String> logData : currentLogData.entrySet() ) {
            String logDataKey = logData.getKey();
            String logDataVal = logData.getValue();

            if(logDataKey != null ){
                if( logDataKey.contains( "Store" ) || logDataKey.contains( "Job" ) || logDataKey.contains( "Service" ) || logDataKey.contains( "Package" ) ){
                    map.put( logDataKey, logDataVal );

                    _LOGGER.finest("Extracting log data. Name: " + logDataKey + ". Value: " + logDataVal );

                }
            }
        }

        return map;

    }

    /**
     * Finds the uuid from the most recent log data
     * */
    private static String getUUID( String logFilePath, int readFromLineNumber ){
        File log = new File( logFilePath );
        String line;
        int lineCounter = 1;
        String uuid;
        Utilities utils = new Utilities();

        try{
            FileReader fr = new FileReader( log );
            BufferedReader br = new BufferedReader( fr );

            while (( line = br.readLine() ) != null) {
                if ( lineCounter >= readFromLineNumber & !line.contains( "Heading" ) ) {
                    if( line.contains( "'MF uuid'" ) ){
                        uuid = utils.textExtraction( "(?<=Value: ')(.*)(?=')", line );
                        fr.close();

                        _LOGGER.info("Log UUID: " + uuid );

                        return uuid;
                    }
                }

                lineCounter++;
            }

        } catch ( IOException e ) {
            _LOGGER.severe( e.getMessage() );

            return null;

        }

        _LOGGER.warning("Failed to find the uuid within current log data set.");

        return null;

    }
}
