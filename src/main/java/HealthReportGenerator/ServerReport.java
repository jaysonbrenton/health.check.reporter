package HealthReportGenerator;

import com.spire.doc.*;
import com.spire.doc.documents.DefaultTableStyle;
import com.spire.doc.documents.Paragraph;
import com.spire.doc.formatting.CharacterFormat;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.logging.Logger;

/**
 * <h1>The ServerReport class is responsible for the population of the report.docx</h1>
 * <p>
 * The ServerReport class will perform the following actions;
 * <ul style=“list-style-type:square”>
 *     <li>Initialising a new Microsoft Word report.docx file using the Reporting Tools template.docx as it basis.</li>
 *     <li>Population of the primary Mediaflux Server report tables within the newly generated report.docx.</li>
 *     <li>Capturing of any warnings that may have been generated during the construction of the Mediaflux Server report tables.</li>
 * </ul>
 *
 * @author  Jayson Brenton
 */
@SuppressWarnings({"CommentedOutCode", "PointlessBooleanExpression", "RedundantIfStatement", "ForLoopReplaceableByForEach"})
public class ServerReport {
    /**
     * The global, properties, jobs, stores, networks & packages configuration data for all servers
     * */
    private final Map<String, String> _globalConfigData;
    private final Map<String, String> _propertiesConfigData;
    private final Map<String, String> _jobsConfigData;
    private final Map<String, String> _storesConfigData;
    private final Map<String, String> _networksConfigData;
    private final Map<String, String> _packagesConfigData;

    /**
     * Lists populated with the log data for a given servers store, network, job & package names or ports when populating the network list
     * Used to establish if there are entries found in the logs that have no corresponding configuration entry.
     * For example, where a given servers log entry is found to have a store called 'store-name-one' but,
     * that server's configuration does not contain a store called 'store-name-one'.
     * */
    private List<String> _storeNamesFromLog = new ArrayList<>();
    private List<String> _networkPortNumberFromLog = new ArrayList<>();
    private List<String> _jobNamesFromLog = new ArrayList<>();
    private List<String> _packageNamesFromLog = new ArrayList<>();
    private List<String> _networkPortNumberConfig = new ArrayList<>();
    private List<String> _jobNamesFromConfig = new ArrayList<>();
    private List<String> _storeNamesFromConfig = new ArrayList<>();
    private List<String> _packageNamesFromConfig = new ArrayList<>();

    /**
     * Multi element log data is defined as any log entries where there are multiple data points that need to be processed.
     * For example, an asset store log entry will appear as follows,
     *      Store name: 'db'
     *      Store Size: '278.24 MB'
     *      Store Free: '441.41 GB'
     *      Store Max-Size: '484.235 GB'
     *      Store Max-Size-Bytes: '484234657792'
     *      Store Free-Percentage: ''
     *      Store Free-DB-Percentage: '91.1562'
     *
     * Single element log data is defined as any log entries where there is a single data element found on a given log entry.
     * For example, Parameter: 'Replication Start Time' Value: '05-Jul-2022 11:44:41'
     * */
    private Map<String, String> _multiElementLogData = null;
    private Map<String, String> _singleElementLogData = null;

    /**
     * Used to instantiate and configure a new spire word document.
     * */
    private Document _document = null;
    private Section _section = null;
    private Table _serverTable = null;
    private Table _summaryTable = null;
    private CharacterFormat _cfCells = null;
    private CharacterFormat _cfCellHeading = null;

    /**
     * Summary data contains a map of warnings. Warnings are created by the build summary data method.
     * */
    private final Map<String, String> _summaryData = new HashMap<>();

    /**
     * Mediaflux server identification information. Populated per server log.
     * Server type will be either primary or secondary
     * */
    private String _serverName = null;
    private String _serverType = null;
    private String _uuid = null;

    /**
     * Used for RAM ceiling warning calculation.
     * */
    private String _mfRAMFreeBytes = null;
    private String _mfRAMMaxBytes = null;
    private String _serverMemoryWarning = null;
    private boolean _serverMemoryChecked;

    private final static Logger _LOGGER = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );
    private final Utilities _utils;

    /**
     * Gets the configuration data for the Current Mediaflux Server
     * */
    public ServerReport(){
        _LOGGER.setLevel( AppLogger._configLogLevel );
        _utils = new Utilities();

        _globalConfigData = new ConfigurationData().globalConfigData();
        _propertiesConfigData = new ConfigurationData().serverPropertiesConfigData();
        _jobsConfigData = new ConfigurationData().jobsConfigData();
        _storesConfigData = new ConfigurationData().storesConfigData();
        _networksConfigData = new ConfigurationData().networksConfigData();
        _packagesConfigData = new ConfigurationData().packagesConfigData();
    }

    /**
     * Return Data
     * */
    /**
     * Call the Word document initialisation.
     * */
    public void initialiseReportDocument(){
        setInitialiseReportDocument();
    }

    /**
     * Sets the current Mediaflux Servers log data. This data is then split into several sub log data objects.
     * Compares the current Mediaflux Servers log data against the corresponding configuration data for that server.
     * */
    public void buildReportDocument( String logFilePath ){
        setBuildReportDocument( logFilePath );

    }

    /**
     * Call the Word document save.
     * */
    public void saveReportDocument( ){
        setSaveReportDocument();
    }

    /**
     * Perform validation and summary table.
     * */
    public void updateSummaryTable( ){
        setUpdateSummaryTable( );

    }

    /**
     * Set Data
     * */
    /**
     * Document initialisation.
     * */
    private void setInitialiseReportDocument(){
        initWordDocument();
    }

    /**
     * Document build.
     * */
    private void setBuildReportDocument( String logFilePath ){
        startDocumentBuild( logFilePath );

    }

    /**
     * Document save.
     * */
    private void setSaveReportDocument( ){
        saveWordDocument();
    }

    /**
     * Document validation.
     * */
    private void setUpdateSummaryTable( ){
        updateSummaryTableText( );

    }

    /**
     * Private class methods
     * */
    /**
     * Create a Word document that will later be populated with the report data
     * If the document's template.file.path is invalid the program will exit.
     * */
    private void initWordDocument(){
        /**
         * Get and check the path to the template document.
         * */
        String templateDocPath = _globalConfigData.get( "template.file.path" );

        if( !_utils.validateTemplate( templateDocPath )){
            _LOGGER.severe("Please provide a valid path to the template document. Provided path: " + templateDocPath );
            System.exit(-1);
        }

        /**
         * Create the Word document
         * */
        _document = new Document( templateDocPath );

        /**
         * Note: we will only ever have a single section in the word template.
         * */
        _section = _document.getSections().get( 0 );

    }

    /**
     * Start the report construction
     * */
    private void startDocumentBuild( String logFilePath ){
        boolean errorFound = false;
        boolean setIDResult;
        String msg = "";

        /**
         * Complete log data is defined as the most recent log entries for a given server log.
         * */
        Map<String, String> completeLogData = new LogData( logFilePath, null ).serverAllLogData();
        if( completeLogData == null ){
            _LOGGER.warning( "Unable to process current log file. Reason: Failed to get log data for current log file.");

        }
        else{
            /**
             * Returns a map of the job / store / network / package log data for the current log file.
             * */
            _multiElementLogData = new LogData( null, completeLogData).multiElementServerLogData();
            if( _multiElementLogData == null){
                errorFound = true;
                msg = msg + "Failed to extract sub-table report data.";

            }

            /**
             * Transforms the keys in the log map to key/value pairs such as mfServerName/VDC-P-Primary rather than Mediaflux Server Name/VDC-P-Primary
             * */
            _singleElementLogData = new LogData( null, completeLogData).singleElementServerLogData();
            if( _singleElementLogData == null){
                errorFound = true;
                msg = msg + "Failed to extract main-table report data.";

            }

            /**
             * Set the Mediaflux Server type. Either Primary or Secondary.
             * */
            _serverType = getServerType();
            if( _serverType == null){
                errorFound = true;
                msg = msg + "Failed to set the server type to either Primary or Secondary.";

            }

            /**
             * Sets _uuid and _serverName which is extracted from the current log data set.
             * Note: data validation performed in setServerNameAndUUID()
             * */
            setIDResult = setServerNameAndUUID();

            /**
             * Return a matching table from _document. The getTable() method will set _table to matched table or null if no matching table found
             * */
            _serverTable = getWordTable( _uuid );
            _summaryTable = getWordTable( "Mediaflux Instance" );

            /**
             * Make sure we have found a table using the current uuid.
             * */
            int testTableRowCount = ( _serverTable == null ) ? 0 : _serverTable.getRows().getCount();

            if( _serverTable == null ){
                /**
                 * No table found.
                 * */
                errorFound = true;
                msg = msg + "Failed to extract the Word table from the template. ";

            }
            else{
                if( testTableRowCount <= 0 ){
                    /**
                     * No table found.
                     * */
                    errorFound = true;
                    msg = msg + "Failed to extract the Word table from the template. ";

                }
            }

            if( setIDResult == false ){
                _LOGGER.severe("Failed to extract the uuid and server name from the current log data. Cannot update report for current log." );

            }
            else{
                if( errorFound == false ){
                    /**
                     * Populate the report
                     * */
                    updateSingleElementTableText( );

                    initMultiElementTableUpdates( );

                    buildSummaryData();

                }
                else{
                    _LOGGER.severe( "Unable to process report for server: " + _serverName + ". Reason: " + msg );

                }
            }
        }
    }

    /**
     * Initiate the building of a servers sub tables.
     * The sub tables that will be built are, Jobs, Stores, Networks & packages
     * */
    private void initMultiElementTableUpdates( ){
        /**
         * Get the row index for the current tables 'Asset Stores', 'Backups' & Network services rows.
         * */
        Map<String, Integer> indexMap = getIndexes();

        /**
         * Get the insertion row index, number of required table columns and rows.
         * */
        Map<String, Integer> rowColumnIndex = buildRowColumnIndex( indexMap );

        /**
         * Sets _cfCells.
         * */
        setTableHeadingFormatting();

        /**
         * Sets _cfCellHeading.
         * */
        setTableCellFormatting();

        /**
         * Create sub tables within the current table.
         * */
        buildStoreSubTable( rowColumnIndex );
        buildJobsSubTable( rowColumnIndex );
        buildNetworksSubTable( rowColumnIndex );
        buildPackagesSubTable( rowColumnIndex );

    }

    /**
     * Builds the stores sub table within the current server table.
     * */
    private void buildStoreSubTable( Map<String, Integer> rowColumnIndex ){
        Table storeNestedTable;
        boolean storeSubTableReset = false;
        int storeRowCount = 1;
        String logDataValue;

        storeNestedTable = _serverTable.get( rowColumnIndex.get( "store.insert.at.row.index" ), 1 ).addTable();
        storeNestedTable.resetCells( rowColumnIndex.get( "store.number.rows" ) + 1, 3 );
        storeNestedTable.autoFit( AutoFitBehaviorType.Auto_Fit_To_Window );
        storeNestedTable.applyStyle( DefaultTableStyle.Table_Grid );

        for ( Map.Entry<String, String> lData : _multiElementLogData.entrySet() ) {
            logDataValue = lData.getValue();

            /**
             * Set up the heading row in the store's sub table
             * */
            if( logDataValue.contains( "Store name" ) && !storeSubTableReset ){
                storeNestedTable = setTableHeadings( "store", storeNestedTable );
                storeSubTableReset = true;

            }

            /**
             * Set up the cell formatting in the store's sub table
             * */
            setTableCellFormatting();

            String mfStoreName;
            String mfStoreSize;
            String mfStoreFreeSpace;
            Map<String, String> storeSubTableMap;

            /**
             * Create a new row in the store's sub table and populates the store data
             * */
            if( logDataValue.contains( "Store name" ) && storeSubTableReset ){
                /**
                 * Build a map of the store data
                 * */
                storeSubTableMap = getSubTableLogData( logDataValue );

                /**
                 * Populate the store sub table with job log data
                 * */
                mfStoreName = storeSubTableMap.get( "mfStoreName" );
                mfStoreSize = storeSubTableMap.get( "mfStoreSize" );
                mfStoreFreeSpace = storeSubTableMap.get( "mfStoreFreeSpace" );

                storeNestedTable.get( storeRowCount, 0 ).addParagraph().appendText( mfStoreName ).applyCharacterFormat( _cfCells );
                storeNestedTable.get( storeRowCount, 1 ).addParagraph().appendText( mfStoreSize ).applyCharacterFormat( _cfCells );
                storeNestedTable.get( storeRowCount, 2 ).addParagraph().appendText( mfStoreFreeSpace ).applyCharacterFormat( _cfCells );

                _LOGGER.finest("Mediaflux Server: " + _serverName +
                        ". Store name: " + mfStoreName +
                        ". Store size: " + mfStoreSize +
                        ". Store free space: " + mfStoreFreeSpace );

                storeRowCount++;

            }
        }
    }

    /**
     * Builds the jobs sub table within the current server table.
     * */
    private void buildJobsSubTable(Map<String, Integer> rowColumnIndex ){
        Table jobNestedTable;
        boolean jobSubTableReset = false;
        int jobRowCount = 1;
        String logDataValue;

        jobNestedTable = _serverTable.get( rowColumnIndex.get( "job.insert.at.row.index" ), 1 ).addTable();
        jobNestedTable.resetCells( rowColumnIndex.get( "job.number.rows" ) + 1, 4 );
        jobNestedTable.autoFit( AutoFitBehaviorType.Auto_Fit_To_Window );
        jobNestedTable.applyStyle( DefaultTableStyle.Table_Grid );

        for ( Map.Entry<String, String> lData : _multiElementLogData.entrySet() ) {
            logDataValue = lData.getValue();

            /**
             * Set up the heading row in the job's sub table
             * */
            if( logDataValue.contains( "Job name" ) && !jobSubTableReset ){
                jobNestedTable = setTableHeadings( "job", jobNestedTable );
                jobSubTableReset = true;
            }

            String mfJobName;
            String mfJobLastExecute;
            String mfJobNextExecute;
            String mfJobExecutePeriod;
            String mfJobWhen;
            String interval;
            Map<String, String> jobSubTableMap;

            /**
             * Create a new row in the job's sub table and populates the store data
             * */
            if( logDataValue.contains( "Job name" ) && jobSubTableReset ){
                /**
                 * Build a map of the job data
                 * */
                jobSubTableMap = getSubTableLogData( logDataValue );

                /**
                 * Populate the job sub table with job log data
                 * */
                mfJobName = jobSubTableMap.get( "mfJobName" );
                mfJobLastExecute = jobSubTableMap.get( "mfJobLastExecute" );
                mfJobNextExecute = jobSubTableMap.get( "mfJobNextExecute" );
                mfJobExecutePeriod = jobSubTableMap.get( "mfJobExecutePeriod" );
                mfJobWhen = jobSubTableMap.get( "mfJobWhen" );
                interval = mfJobExecutePeriod + ",\n" + mfJobWhen ;

                jobNestedTable.get( jobRowCount, 0 ).addParagraph().appendText( mfJobName ).applyCharacterFormat( _cfCells );
                jobNestedTable.get( jobRowCount, 1 ).addParagraph().appendText( mfJobLastExecute ).applyCharacterFormat( _cfCells );
                jobNestedTable.get( jobRowCount, 2 ).addParagraph().appendText( mfJobNextExecute ).applyCharacterFormat( _cfCells );
                jobNestedTable.get( jobRowCount, 3 ).addParagraph().appendText( interval ).applyCharacterFormat( _cfCells );

                _LOGGER.finest("Mediaflux Server: " + _serverName +
                        ". Job name: " + mfJobName +
                        ". Last execute: " + mfJobLastExecute +
                        ". Next execute: " + mfJobNextExecute );

                jobRowCount++;

            }
        }
    }

    /**
     * Builds the networks sub table within the current server table.
     * */
    private void buildNetworksSubTable(Map<String, Integer> rowColumnIndex ){
        Table svcNestedTable;
        boolean svcSubTableReset = false;
        int svcRowCount = 1;
        String logDataValue;
        Map<String, String> svcSubTableMap;

        svcNestedTable = _serverTable.get( rowColumnIndex.get( "svc.insert.at.row.index" ), 1 ).addTable();
        svcNestedTable.resetCells( rowColumnIndex.get( "svc.number.rows" ) + 1, 6 );
        svcNestedTable.autoFit( AutoFitBehaviorType.Auto_Fit_To_Window );
        svcNestedTable.applyStyle( DefaultTableStyle.Table_Grid );

        for ( Map.Entry<String, String> lData : _multiElementLogData.entrySet() ) {
            logDataValue = lData.getValue();

            /**
             * Set up the heading row in the network's sub table
             * */
            if( logDataValue.contains( "Service port" ) && !svcSubTableReset ){
                svcNestedTable = setTableHeadings( "network", svcNestedTable );
                svcSubTableReset = true;
            }

            String mfServicePort;
            String mfServiceType;
            String mfServiceSSL;
            String mfServiceStart;
            String mfServicePeak;
            String mfServicePeakAt;

            /**
             * Create a new row in the network's sub table and populates the store data
             * */
            if( logDataValue.contains( "Service port" ) && svcSubTableReset ){
                /**
                 * Build a map of the network data
                 * */
                svcSubTableMap = getSubTableLogData( logDataValue );

                /**
                 * Populate the job network table with job log data
                 * */
                mfServicePort = svcSubTableMap.get( "mfServicePort" );
                mfServiceType = svcSubTableMap.get( "mfServiceType" );
                mfServiceSSL = svcSubTableMap.get( "mfServiceSSL" );
                mfServiceStart = svcSubTableMap.get( "mfServiceStart" );
                mfServicePeak = svcSubTableMap.get( "mfServicePeak" );
                mfServicePeakAt = svcSubTableMap.get( "mfServicePeakAt" );

                svcNestedTable.get( svcRowCount, 0 ).addParagraph().appendText( mfServicePort ).applyCharacterFormat( _cfCells );
                svcNestedTable.get( svcRowCount, 1 ).addParagraph().appendText( mfServiceType ).applyCharacterFormat( _cfCells );
                svcNestedTable.get( svcRowCount, 2 ).addParagraph().appendText( mfServiceSSL ).applyCharacterFormat( _cfCells );
                svcNestedTable.get( svcRowCount, 3 ).addParagraph().appendText( mfServiceStart ).applyCharacterFormat( _cfCells );
                svcNestedTable.get( svcRowCount, 4 ).addParagraph().appendText( mfServicePeak ).applyCharacterFormat( _cfCells );
                svcNestedTable.get( svcRowCount, 5 ).addParagraph().appendText( mfServicePeakAt ).applyCharacterFormat( _cfCells );

                _LOGGER.finest("Mediaflux Server: " + _serverName +
                        ". Port: " + mfServicePort +
                        ". Type: " + mfServiceType +
                        ". SSL: " + mfServiceSSL +
                        ". Started: " + mfServiceStart);

                svcRowCount++;
            }
        }

    }

    /**
     * Builds the packages sub table within the current server table.
     * */
    private void buildPackagesSubTable(Map<String, Integer> rowColumnIndex ){
        Table pacNestedTable;
        boolean pacSubTableReset = false;
        int pacRowCount = 1;
        String logDataValue;
        Map<String, String> pacSubTableMap;

        pacNestedTable = _serverTable.get( rowColumnIndex.get( "pac.insert.at.row.index" ), 1 ).addTable();
        pacNestedTable.resetCells( rowColumnIndex.get( "pac.number.rows" ) + 1, 2 );
        pacNestedTable.autoFit( AutoFitBehaviorType.Auto_Fit_To_Window );
        pacNestedTable.applyStyle( DefaultTableStyle.Table_Grid );

        for ( Map.Entry<String, String> lData : _multiElementLogData.entrySet() ) {
            logDataValue = lData.getValue();

            /**
             * Set up the heading row in the package's sub table
             * */
            if( logDataValue.contains( "Package name" ) && !pacSubTableReset ){
                pacNestedTable = setTableHeadings( "package", pacNestedTable );
                pacSubTableReset = true;

            }

            String mfPackageName;
            String mfPackageVersion;

            /**
             * Create a new row in the package's sub table and populates the store data
             * */
            if( logDataValue.contains( "Package name" ) && pacSubTableReset ){
                /**
                 * Build a map of the package data
                 * */
                pacSubTableMap = getSubTableLogData( logDataValue );

                /**
                 * Populate the package sub table with job log data
                 * */
                mfPackageName = pacSubTableMap.get( "mfPackageName" );
                mfPackageVersion = pacSubTableMap.get( "mfPackageVersion" );

                pacNestedTable.get( pacRowCount, 0 ).addParagraph().appendText( mfPackageName ).applyCharacterFormat( _cfCells );
                pacNestedTable.get( pacRowCount, 1 ).addParagraph().appendText( mfPackageVersion ).applyCharacterFormat( _cfCells );

                _LOGGER.finest("Mediaflux Server: " + _serverName +
                        ". Package name: " + mfPackageName +
                        ". Package version: " + mfPackageVersion );

                pacRowCount++;

            }
        }
    }

    /**
     * Update the summary table
     * */
    private void updateSummaryTableText(){
        String summaryDataValue;
        String summaryDataKey;
        String configurationSettingName;
        String configurationSettingValue;
        String mfServerName;
        String logValue;
        boolean testResult;
        String[] testData;
        String packageVersion = null;
        String packageName = null;

        for ( Map.Entry<String, String> sumData : _summaryData.entrySet() ) {
            summaryDataValue = sumData.getValue();
            summaryDataKey = sumData.getKey();

            configurationSettingName = _utils.textExtraction( "(?<=Configuration Name:)(.*)(?=Configuration Value:)", summaryDataValue );
            configurationSettingValue = _utils.textExtraction( "(?<=Configuration Value:)(.*)(?=Log Value:)", summaryDataValue );
            mfServerName = _utils.textExtraction("(?<=)(.*)(?=##)", summaryDataKey );
            logValue = _utils.textExtraction("(?<=Log Value:)(.*)(?=)", summaryDataValue );

            /**
             * Test required data exists
             * */
            if( configurationSettingName.contains( "config.store.name.not.found.in.log" )){
                testData = new String[]{ configurationSettingName, configurationSettingValue };
            }
            else if( configurationSettingName.contains( "log.store.name.not.found.in.config" )){
                testData = new String[]{ configurationSettingName, logValue };
            }
            else if( configurationSettingName.contains( "config.job.name.not.found.in.log" )){
                testData = new String[]{ configurationSettingName, configurationSettingValue };
            }
            else if( configurationSettingName.contains( "log.package.name.not.found.in.config" )){
                testData = new String[]{ configurationSettingName, logValue };
            }
            else if( configurationSettingName.contains( "config.package.name.not.found.in.log" )){
                packageName = _utils.textExtraction( "(?<=Configuration Value: )(.*)(?=\\sPackage version:)", summaryDataValue );
                packageVersion = _utils.textExtraction( "(?<=\\sPackage version:)(.*)(?=)", summaryDataValue );
                testData = new String[]{ configurationSettingName, packageName, packageVersion };
            }
            else{
                testData = new String[]{ configurationSettingName, configurationSettingValue, logValue };
            }

            /**
             * Perform test
             * */
            testResult = _utils.testData( testData );

            /**
             * Perform summary table text updates
             * */
            if( testResult == true ){
                /**
                 * Build summary messages where required
                 * */
                if( configurationSettingName.contains( "store.ceiling.warning" )){
                    String storeName = logValue.substring( logValue.indexOf( ": " ) + 2 ).trim();
                    configurationSettingValue = "More than " + configurationSettingValue + "% free on store: " + storeName;
                    configurationSettingName = "store.ceiling.warning";
                    logValue = logValue.substring( 0 , logValue.indexOf( "S" ) -1 ).trim() + "% free";

                }
                else if( configurationSettingName.contains( "peak.warning" )){
                    String portNum = configurationSettingName.substring( 0, configurationSettingName.indexOf( "peak.warning" ) - 1 );
                    configurationSettingName = "network.peak.warning";
                    configurationSettingValue = "Connections should be less than " + configurationSettingValue + ", on port: " + portNum;
                    logValue = logValue + " connections";

                }
                else if( configurationSettingName.contains( "rep.duration" )){
                    configurationSettingValue = "Replication time should be less than " + configurationSettingValue + " milliseconds";
                    logValue = logValue + " milliseconds";

                }
                else if( configurationSettingName.contains( "ram.ceiling.warning" ) ){
                    configurationSettingValue = configurationSettingValue.substring(0, configurationSettingValue.indexOf(".")) + "%";
                    configurationSettingValue = "Available RAM should be more than: " + configurationSettingValue;
                    logValue = logValue.substring(0, logValue.indexOf(".")) + "%";
                    logValue = "Runtime RAM Free is: " + logValue;
                }
                else if( configurationSettingName.contains( "network." ) ){
                    String portNumber = _utils.textExtraction( "(?<=##)(.*)(?=_)", summaryDataKey );
                    configurationSettingName = configurationSettingName + " on port: " + portNumber;
                }
                else if( configurationSettingName.contains( "config.package.name.not.found.in.log" ) ){
                    configurationSettingValue = "Package name: " + packageName + "\nPackage version: " + packageVersion;
                }

                _LOGGER.finest("Update summary table. Mediaflux server name: " + mfServerName +
                        " Configuration setting name: " + configurationSettingName +
                        " Configuration setting value: " + configurationSettingValue +
                        " Log value: " + logValue );

                int rowNumber = _summaryTable.addRow(4).getRowIndex();
                _summaryTable.getRows().get( rowNumber ).getCells().get(0).addParagraph().appendText( mfServerName ).applyCharacterFormat( _cfCells );
                _summaryTable.getRows().get( rowNumber ).getCells().get(1).addParagraph().appendText( configurationSettingName ).applyCharacterFormat( _cfCells );
                _summaryTable.getRows().get( rowNumber ).getCells().get(2).addParagraph().appendText( configurationSettingValue ).applyCharacterFormat( _cfCells );
                _summaryTable.getRows().get( rowNumber ).getCells().get(3).addParagraph().appendText( logValue ).applyCharacterFormat( _cfCells );
                _summaryTable.autoFit( AutoFitBehaviorType.Auto_Fit_To_Window );
            }
        }
    }

    /**
     * Performs the population of the server tables in the report
     * Search for key's in the current table and replace with log data when matched.
     * For example, each table in template will contain a "${mfVersion}" value.
     * This will be matched to the Mediaflux server version discovered in a log file.
     * */
    private void updateSingleElementTableText( ){
        int elementCounter = 0;
        for(TableRow row:(Iterable<TableRow>) _serverTable.getRows()){
            for(TableCell cell : (Iterable<TableCell>)row.getCells()){
                for(Paragraph para : (Iterable<Paragraph>)cell.getParagraphs()){
                    for (Map.Entry<String, String> entry : _singleElementLogData.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();

                        int valLen = ( value == null ) ? 0 : value.length();
                        if( valLen > 0 ){
                            para.replace("${" + key + "}", value, false, true);

                        }else{
                            para.replace("${" + key + "}", "", false, true);

                        }

                        if( elementCounter <= _singleElementLogData.size() ){
                            _LOGGER.finest("Update report table. Log setting name: " + key + ". Log setting value: " + value );
                        }

                        elementCounter++;
                    }
                }
            }
        }
    }

    /**
     * Search the Word document and locate table
     * */
    private Table getWordTable( String lookupKey ){
        String currentKey;

        /**
         * Iterate the tables in the Word document
         * */
        for( Table table : (Iterable<Table>) _section.getTables() ){
            String currTableIdentifier = table.getRows().get( 0 ).getCells().get( 0 ).getParagraphs().get( 0 ).getText().trim();

            if( currTableIdentifier.contains( "Mediaflux Instance" ) && lookupKey.contains( "Mediaflux Instance" ) ) {
                /**
                 * Return the summary table
                 * */
                _LOGGER.finest("Discovered summary table.");

                return table;

            }else if( currTableIdentifier.contains( "Activity" ) ) {
                String id = table.get(2, 0).getParagraphs().get( 0 ).getText().trim();
                currentKey = _utils.uniqueIDS( id );

                if( currentKey.equals( lookupKey ) ){
                    /**
                     * Return a server table
                     * */
                    _LOGGER.finest("Discovered report table. UUID: " + currentKey );
                    return table;

                }
            }
        }

        _LOGGER.warning("Failed to discover word table.");
        return null;
    }

    /**
     * Saves the final report document
     * */
    private void saveWordDocument(){
        String reportFilePath = _globalConfigData.get( "report.directory.path" );
        String reportFileName = _globalConfigData.get( "report.file.name" );

        /**
         * Creates the path to the final report document
         * */
        String currDate;
        if( reportFileName.contains( "${add-create-date}" ) ){
            currDate = _utils.currentDate().toUpperCase( Locale.ROOT );;
            reportFileName = reportFileName.replace("${add-create-date}", currDate);
        }

        /**
         * replace the date on page one of report
         * */
        String d = _utils.formatDate();
        _document.replace("${date}", d, false, true);

        /**
         * Save the report document
         * */
        String fullReportPath = reportFilePath + reportFileName;
        _document.saveToFile( fullReportPath, FileFormat.Docx_2013 );

        _LOGGER.finest("Saved report document to: " + reportFilePath + reportFileName );
    }

    /**
     * Sets the sub table cell formatting.
     * */
    private void setTableCellFormatting( ){
        CharacterFormat cfCells = new CharacterFormat( _document );
        cfCells.setFontName( "Consolas" );
        cfCells.setFontSize( 8 );
        cfCells.setTextColor( Color.gray );
        cfCells.setBold( true );

        _cfCells = cfCells;

    }

    /**
     * Sets the sub table heading row formatting.
     * */
    private void setTableHeadingFormatting( ){
        CharacterFormat cfCellHeading = new CharacterFormat( _document );
        cfCellHeading.setFontName( "Helvetica" );
        cfCellHeading.setFontSize( 8 );
        cfCellHeading.setTextColor( Color.white );
        cfCellHeading.setBold( true );

        _cfCellHeading = cfCellHeading;

    }

    /**
     * Returns the row/column index for the backups, networks, packages and asset stores rows on a given table.
     * */
    private LinkedHashMap<String, Integer> getIndexes( ){
        LinkedHashMap<String, Integer> newMap = new LinkedHashMap<>();

        for(TableRow row:(Iterable<TableRow>) _serverTable.getRows()){
            for(TableCell cell : (Iterable<TableCell>)row.getCells()){

                for(Paragraph para : (Iterable<Paragraph>)cell.getParagraphs()){
                    String cellText = para.getText().trim();
                    int rowIdx = row.getRowIndex();

                    if( cellText.contains( "Asset Stores" ) ){
                        newMap.put( rowIdx + ".store.insert.at.row.index", rowIdx );
                        newMap.put( rowIdx + ".store.number.rows.", _utils.rowCount( _multiElementLogData ).get( "store" ) );

                    }else if( cellText.contains( "Backups" ) ){
                        newMap.put( rowIdx + ".job.insert.at.row.index.", rowIdx );
                        newMap.put( rowIdx + ".job.number.rows.", _utils.rowCount( _multiElementLogData ).get( "job" ) );

                    }else if( cellText.contains( "Networks" ) ){
                        newMap.put( rowIdx + ".svc.insert.at.row.index.", rowIdx );
                        newMap.put( rowIdx + ".svc.number.rows.", _utils.rowCount( _multiElementLogData ).get( "service" ) );

                    }else if( cellText.contains( "Package" ) ){
                        newMap.put( rowIdx + ".pac.insert.at.row.index.", rowIdx );
                        newMap.put( rowIdx + ".pac.number.rows.", _utils.rowCount( _multiElementLogData ).get( "package" ) );

                    }
                }
            }
        }

        return newMap;

    }

    /**
     * Returns a map of either the job, package, service or store log data.
     * */
    private Map<String, String> getSubTableLogData(String value){
        Map<String, String> newMap = new LinkedHashMap<>();
        String result;

        if( value.contains( "Store" ) ) {
            if ( value.contains( "Store name:" ) ) {
                result = _utils.textExtraction("(?<=Store name: ')(.*)(?=' Store Size:)", value );
                newMap.put( "mfStoreName", result );

            }

            if ( value.contains( "Store Size:" ) ) {
                result = _utils.textExtraction("(?<=Store Size: ')(.*)(?=' Store Free:)", value );
                newMap.put( "mfStoreSize", result );

            }

            if ( value.contains( "Store Free:" ) ) {
                result = _utils.textExtraction("(?<=Store Free: ')(.*)(?=' Store Max-Size:)", value );
                newMap.put( "mfStoreFreeSpace", result );

            }

            if ( value.contains( "Store Max-Size" ) ) {
                result = _utils.textExtraction("(?<=Store Max-Size: ')(.*)(?=' Store Max-Size-Bytes:)", value );
                newMap.put( "mfStoreMaxSize", result );

            }

            if ( value.contains( "Store Max-Size-Bytes" ) ) {
                result = _utils.textExtraction( "(?<=Store Max-Size-Bytes: ')(.*)(?=' Store Free-Percentage)", value );
                newMap.put( "mfStoreMaxSizeBytes", result );

            }

            if ( value.contains( "Store Free-DB-Percentage" ) ) {
                result = _utils.textExtraction( "(?<=Store Free-DB-Percentage: ')(.*)(?=')", value );
                newMap.put( "mfStoreFreeDBPercentage", result );

            }
        }

        if( value.contains( "Job" ) ) {
            if ( value.contains( "Job name:" ) ) {
                result = _utils.textExtraction( "(?<=Job name: ')(.*)(?=' Job last execution:)", value );
                newMap.put( "mfJobName", result );
            }

            if ( value.contains( "Job last execution:" ) ) {
                result = _utils.textExtraction( "(?<=Job last execution: ')(.*)(?=' Job next execution:)", value );
                newMap.put( "mfJobLastExecute", result );

            }

            if ( value.contains( "Job next execution:" ) ) {
                result = _utils.textExtraction( "(?<=Job next execution: ')(.*)(?=' Job execution period:)", value );
                newMap.put( "mfJobNextExecute", result );

            }

            if ( value.contains( "Job execution period:" ) ) {
                result = _utils.textExtraction( "(?<=Job execution period: )(.*)(?= Job execution when:)", value );
                newMap.put( "mfJobExecutePeriod", result );

            }

            if ( value.contains( "Job execution when:" ) ) {
                result = _utils.textExtraction( "(?<=Job execution when: ')(.*)(?=')", value );
                newMap.put( "mfJobWhen", result );

            }
        }

        if( value.contains( "Service" ) ) {
            if ( value.contains( "Service port:" ) ) {
                result = _utils.textExtraction("(?<=Service port: ')(.*)(?=. Service type:)", value );
                newMap.put( "mfServicePort", result );
            }
            if ( value.contains( "Service type:" ) ) {
                result = _utils.textExtraction( "(?<=Service type: ')(.*)(?=' Service ssl:)", value );
                newMap.put( "mfServiceType", result );
            }
            if ( value.contains( "Service ssl:" ) ) {
                result = _utils.textExtraction( "(?<=Service ssl: ')(.*)(?=' Service trust:)", value );
                newMap.put( "mfServiceSSL", result );
            }
            if ( value.contains( "Service start:" ) ) {
                result = _utils.textExtraction( "(?<=Service start: ')(.*)(?=' Service peak:)", value );
                newMap.put( "mfServiceStart", result );
            }
            if ( value.contains( "Service peak:" ) ) {
                result = _utils.textExtraction( "(?<=Service peak: ')(.*)(?=' Service peak-at:)", value );
                newMap.put( "mfServicePeak", result );
            }
            if ( value.contains( "Service peak-at:" ) ) {
                result = _utils.textExtraction( "(?<=Service peak-at: ')(.*)(?=')", value );
                newMap.put( "mfServicePeakAt", result );

            }

        }

        if( value.contains( "Package" ) ) {
            if ( value.contains( "Package name:" ) ) {
                result = _utils.textExtraction( "(?<=Package name: ')(.*)(?=' Package version:)", value );
                if(!result.equals("")){
                    newMap.put( "mfPackageName", result );

                }

            }
            if ( value.contains( "Package version:" ) ) {
                result = _utils.textExtraction( "(?<=Package version: ')(.*)(?=')", value );
                newMap.put( "mfPackageVersion", result );
            }
        }

        return newMap;
    }

    /**
     * Iterate the log data within the current log file
     * Perform test to ensure the log data has been supplied.
     * If log data supplied, call the single element data validation.
     * */
    private void buildSummaryData( ){
        String logSettingName;
        String logDataValue;
        boolean dataTestResult = false;
        _mfRAMFreeBytes = null;
        _mfRAMMaxBytes = null;
        _serverMemoryWarning = null;
        _serverMemoryChecked = false;

        /**
         * Exclude these log entries from the single element data validation.
         * Note: This is so we don't validate replication log data for secondary MF servers.
         * */
        String termList = String.valueOf( Arrays.asList(
                "mfRepDuration", "mfRepStart", "mfRepEnd", "mfRepFailed", "xodb.status.rep.duration.warning" )
        );

        /**
         * Iterate the current logs data and call the single element validation
         * */
        for ( Map.Entry<String, String> lData : _singleElementLogData.entrySet() ) {
            logSettingName = lData.getKey();
            logDataValue = lData.getValue();

            /**
             * Test the input log data.
             * Note: For a secondary server we will never have replication related log data therefor we must exclude those from the data test for secondaries
             * */
            String[] testLogData = new String[]{ logSettingName, logDataValue };

            /**
             * Perform data exists tests
             * */
            if( _serverType.equals( "secondary" ) ) {
                /**
                 * Note: For secondary server's, we do not want to test replication related settings
                 * */
                if( !termList.contains( logSettingName ) ){
                    dataTestResult =  _utils.testDataExistence( testLogData );

                }
            }
            else{
                /**
                 * Test the input log data.
                 * Note: Not secondary therefor all log data elements should be validated unlike secondaries.
                 * */
                dataTestResult =  _utils.testDataExistence( testLogData );

            }

            /**
             * Perform the data validation
             * */
            if( dataTestResult == true ){
                singleElementDataValidation( logSettingName, logDataValue );

            }
        }

        /**
         * Start the multi element validation
         * */
        multiElementDataValidation();
    }

    /**
     * Validates the single element log data against the configuration data for the corresponding MF server
     * */
    private void singleElementDataValidation( String logSettingName, String logDataValue ){
        boolean dataTestResult;

        /**
         * Exclude the following config setting from the iteration of the current server's config data.
         * Note: RAM total is tested below however, RAM warnings (ceilings) calculated separately from the other configuration items.
         * */
        String termList = String.valueOf( Arrays.asList(
                "mfRAMUsed", "mfRAMFree", "mfRAMTotalBytes", "mfRAMUsedBytes", "mfRAMFreeBytes", "mfRAMMaxBytes", "log.store.name.not.found.in.config" )
        );

        /**
         * Used for server memory warning calculation
         * */
        if( logSettingName.equals( "mfRAMFreeBytes" )){
            _mfRAMFreeBytes = logDataValue;

        }

        /**
         * Used for server memory warning calculation
         * */
        if ( logSettingName.equals( "mfRAMMaxBytes" ) ){
            _mfRAMMaxBytes = logDataValue;

        }

        /**
         * Iterate the server's configuration data and validate against supplied log data
         * */
        if( !termList.contains( logSettingName ) || logSettingName.equals( "mfRAMTotal" ) ){
            /**
             * Extract data from the _propertiesConfigData where the current log data's, uuid, matches that sound in _propertiesConfigData
             * Results in _perServerPropertiesConfigData containing only the configuration data that matches the current log's uuid
             * */
            Map<String, String> _perServerPropertiesConfigData = extractConfigurationData("propertiesConfigData");
            String settingName;
            String settingValue;

            /**
             * Iterate the current set of configuration data and validate against supplied log setting name's and values.
             * */
            for ( Map.Entry<String, String> cData : _perServerPropertiesConfigData.entrySet() ) {
                String cDataVal = cData.getValue().trim();

                /**
                 * Sets the configuration setting name.
                 * */
                settingName = _utils.textExtraction( "(?<=.*)(.*)(?=:)", cDataVal  );
                //settingName = utils.textExtraction( "(?<=.*)(.*)(?=:)", cDataVal );

                /**
                 * Sets the configuration setting value.
                 * */
                settingValue = _utils.textExtraction( "(?<=.*:)(.*)(?=.*)", cDataVal  );
                //settingValue = utils.textExtraction( "(?<=.*:)(.*)(?=.*)", cDataVal );

                String[] testConfigData = new String[]{ settingName, settingValue };
                dataTestResult = _utils.testDataExistence( testConfigData );
                //dataTestResult = Utilities.testDataExists( testConfigData );

                if( dataTestResult == true ){
                    if( settingName.equals( "server.memory.warning" ) && _serverMemoryWarning == null ){
                        /**
                         * Used for server memory warning calculation
                         * */
                        _serverMemoryWarning = settingValue;

                    }

                    if(logSettingName.equals("mfRepFailed")){
                        /**
                         * No config setting for replication failure. Anything greater than 0 is an issue
                         * */
                        settingValue = "0";
                    }

                    /**
                     * Start the validation.
                     * */
                    boolean res = initSingleElementDataChecks( logDataValue, logSettingName, settingValue, settingName );

                    if( res == true ){
                        // Checks completed. No need to continue iterating the configuration
                        break;
                    }
                }
            }

            /**
             * Test that _mfRAMMaxBytes, _mfRAMFreeBytes & _serverMemoryWarning have been populated.
             * */
            String[] testMemData = new String[]{ _mfRAMMaxBytes, _mfRAMFreeBytes, _serverMemoryWarning };
            //dataTestResult =  Utilities.testDataExists( testMemData );
            dataTestResult = _utils.testData( testMemData );

            /**
             * Test the memory ceiling
             * Now that we have completed iterating the passed in log data, we should have _mfRAMMaxBytes, _mfRAMFreeBytes & _serverMemoryWarning
             * */
            if( _serverMemoryChecked == false ){
                if( dataTestResult == true ){
                    double percentFree = _utils.percentageCalculator( _mfRAMMaxBytes, _mfRAMFreeBytes );
                    double memWarning = Utilities.getDouble( _serverMemoryWarning );
                    if( percentFree < memWarning ){
                        _summaryData.put( _serverName + "##ram.ceiling.warning",
                                "Configuration Name: ram.ceiling.warning Configuration Value: " + memWarning + " Log Value: " + percentFree);
                    }

                    _serverMemoryChecked = true;
                }

            }
        }
    }

    /**
     * Initiates the validation of the multi-value log data elements.
     * Those being, stores, jobs, network & package log data
     * */
    private void multiElementDataValidation( ){

        /**
         * Initiates the multi element log data and calls methods responsible for setting up and performing the updates to the _summaryData map.
         * */
        for ( Map.Entry<String, String> lData : _multiElementLogData.entrySet() ) {
            String logValue = lData.getValue();

            if( logValue.contains( "Store name:" ) ){
                validateStoreData( logValue );

            }
            else if( logValue.contains( "Service port:" ) ){
                validateNetworkData( logValue );

            }
            else if( logValue.contains( "Job name:" ) ){
                validateJobData( logValue );

            }
            else if( logValue.contains( "Package name:" ) ){
                //System.out.println( "Package Log Data. " + logValue );
                validatePackageData( logValue );

            }
        }

        /**
         * Compare the list of log elements that do not appear within the configuration
         * */
        compareLists();

        /**
         * Reset all the globals used per log file
         * */
        _storeNamesFromConfig = null;
        _jobNamesFromConfig = null;
        _packageNamesFromConfig = null;
        _networkPortNumberConfig = null;
        _packageNamesFromLog = null;
        _jobNamesFromLog = null;
        _storeNamesFromLog = null;
        _networkPortNumberFromLog = null;
    }

    /**
     * Matches a log setting name to it's corresponding configuration setting name. When matched, call checkLogForErrors() for data validation
     * */
    private boolean initSingleElementDataChecks( String logDataValue, String logSettingName, String confDataValue, String confSettingName ){
        boolean validationResult = true;
        boolean matchedSetting = false;

        if( logSettingName.equals( "mfRepDuration" ) && confSettingName.equals( "xodb.status.rep.duration.warning" ) ){
            if( _propertiesConfigData.containsValue( "is.primary.mf.server:true" ) ){
                validationResult = checkLogForErrors( logDataValue, confDataValue, ">" );
                matchedSetting = true;

            }

        }
        else if( logSettingName.equals( "mfVersion") && confSettingName.equals( "mediaflux.server.version" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, "" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "numCPUs" ) && confSettingName.equals( "number.of.cpus" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, "<" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "osArch" ) && confSettingName.equals( "os.architecture" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, "" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "mfServerAvgLoad" ) && confSettingName.equals( "server.status.load.warning" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, ">" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "osName" ) && confSettingName.equals( "host.os.name" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, "" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "mfNumOpenStreams" ) && confSettingName.equals( "server.status.nbopen.warning" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, ">" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "mfServerUpTime" ) && confSettingName.equals( "mf.server.uptime" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, "<" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "osVersion" ) && confSettingName.equals( "host.os.version" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, "" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "mfRAMTotal" ) && confSettingName.equals( "ram.in.gigabytes" ) ){
            validationResult = checkLogForErrors( logDataValue, confDataValue, "<" );
            matchedSetting = true;

        }
        else if( logSettingName.equals( "mfRepFailed" ) ){
            validationResult = checkLogForErrors( logDataValue, "0", ">" );
            confSettingName = "replication.failure";
            matchedSetting = true;

        }

        if( matchedSetting == true && validationResult == false ){
            // We have matched a log setting to a config setting. Validation complete
            _LOGGER.warning("Server name: " + _serverName +
                    ". Configuration name: " + confSettingName +
                    ". Configuration Value: " + confDataValue +
                    ". Log Value: " + logDataValue );

            _summaryData.put(
                    _serverName + "##" + confDataValue,
                    "Configuration Name: " + confSettingName + " Configuration Value: " + confDataValue + " Log Value: " + logDataValue );

            return true;

        }
        else if( matchedSetting == true ){
            // We have matched a log setting to a config setting. Validation complete
            return true;

        }

        return false;
    }

    // Perform data validation checks
    private boolean checkLogForErrors( String logValue, String configValue, String comparisonDirection ){
        double lVal;
        double cVal;
        boolean raiseWarning = false;

        _LOGGER.finest("Log value: " + logValue + ". Config value: " + configValue + ". Comparison direction (numbers only): " + comparisonDirection );

        if( Utilities.isNumeric( logValue ) && Utilities.isNumeric( configValue ) ){
            lVal = Utilities.getDouble( logValue );
            cVal = Utilities.getDouble( configValue );

            if(comparisonDirection.contains( ">" )){
                if( lVal > cVal ){
                    raiseWarning = true;
                }

            }else{
                if( lVal < cVal ){
                    raiseWarning = true;
                }

            }
        }else{
            // Comparing strings
            if( !configValue.toUpperCase().trim().matches( logValue.toUpperCase().trim() ) ){
                raiseWarning = true;
            }
        }

        if( raiseWarning ){
            return false;

        }
        else{
            return true;

        }
    }

    /**
     * Returns either "primary" or "secondary".
     * Defined in HealthReportGenerator.xml, is.primary.mf.server:true/false
     * */
    private String getServerType(){
        String serverType;

        if( _propertiesConfigData.containsValue( "is.primary.mf.server:true" ) ){
            _LOGGER.finest("Is primary server: true." );
            serverType = "primary";

        }
        else if( _propertiesConfigData.containsValue( "is.primary.mf.server:false" ) ){
            _LOGGER.finest("Is primary server: false." );
            serverType = "secondary";

        }
        else{
            _LOGGER.severe( "Unable to determine if current Mediaflux server is primary or secondary.");
            return null;

        }

        return serverType;
    }

    /**
     * Sets the MF host name and uuid using the current log map
     * */
    private boolean setServerNameAndUUID(){
        boolean testResult;
        Utilities utils = new Utilities();

        _serverName = utils.textExtraction( "(?<=UUID: \\d\\d\\d\\d\\.)(.*)(?=)", _singleElementLogData.get( "mfServerIdentifier" ) );
        _uuid = utils.textExtraction( "(?<=.*)(\\d+)(?=.*)", _singleElementLogData.get( "mfServerIdentifier" ) );

        String[] testLogData = new String[]{ _serverName, _uuid };

        /**
         * Test that we have MF host name and uuid
         * */
        //testResult =  Utilities.testDataExists( testLogData );
        testResult = _utils.testData( testLogData );

        if( testResult == false ){
            return false;

        }
        else{
            /**
             * Test uuid is valid numeric
             * */
            testResult = Utilities.isNumeric( _uuid );

            return testResult;

        }

    }

    /**
     * Extract the config data using the current log files uuid as a matcher.
     * Note: The config data map contains the uuid in the key. This is what is being matched.
     * */
    private Map<String, String> extractConfigurationData( String dataSet ){
        String configSettingName;
        String configDataValue;
        String uuid;
        Map<String, String> tempMap  = new HashMap<>();
        Set<Map.Entry<String, String>> entry = null;

        /**
         * Get the required configuration data set.
         * */
        switch( dataSet ){
            case "propertiesConfigData":
                entry = _propertiesConfigData.entrySet();
                break;
            case "storeConfigData":
                entry = _storesConfigData.entrySet();
                break;
            case "networkConfigData":
                entry = _networksConfigData.entrySet();
                break;
            case "jobConfigData":
                entry = _jobsConfigData.entrySet();
                break;
            case "packageConfigData":
                entry = _packagesConfigData.entrySet();
                break;
            default:
                _LOGGER.severe("Unexpected value: " + dataSet );
                //throw new IllegalStateException( "Unexpected value: " + dataSet );
        }

        /**
         * Extract the required per uuid, configuration data.
         * */
        for ( Map.Entry<String, String> pData : entry ) {
            configSettingName = pData.getKey();
            configDataValue = pData.getValue();
            uuid = configSettingName.substring( 0, configSettingName.indexOf("_") );

            if( _uuid.equals( uuid )){
                _LOGGER.finest("Update configuration data for current log. Setting name: " + configSettingName + ". Setting value: " + configDataValue );
                tempMap.put( configSettingName, configDataValue );

            }
        }

        /**
         * Return the new map which will contain the configuration data for the currently processed uuid.
         * */
        return tempMap;
    }

    // Get the insertion row index, number of required table columns and rows.
    private static Map<String, Integer> buildRowColumnIndex( Map<String, Integer> indexMap ){
        Map<String, Integer> map = new HashMap<>();

        for ( Map.Entry<String, Integer> rowIndex : indexMap.entrySet() ) {
            String key = rowIndex.getKey();
            int value = rowIndex.getValue();

            if( key.contains("store.insert.at.row.index") ){
                map.put( "store.insert.at.row.index", value);

            }else if( key.contains("store.number.columns") ){
                map.put( "store.number.columns", value);

            }else if( key.contains("store.number.rows") ){
                map.put( "store.number.rows", value);

            }else if( key.contains("job.insert.at.row.index") ){
                map.put( "job.insert.at.row.index", value);

            }else if( key.contains("job.number.columns") ){
                map.put( "job.number.columns", value);

            }else if( key.contains("job.number.rows") ){
                map.put( "job.number.rows", value);

            }else if( key.contains("svc.insert.at.row.index") ){
                map.put( "svc.insert.at.row.index", value);

            }else if( key.contains("svc.number.columns") ){
                map.put( "svc.number.columns", value);

            }else if( key.contains("svc.number.rows") ){
                map.put( "svc.number.rows", value);

            }else if( key.contains("pac.insert.at.row.index") ){
                map.put( "pac.insert.at.row.index", value);

            }else if( key.contains("pac.number.columns") ){
                map.put( "pac.number.columns", value);

            }else if( key.contains("pac.number.rows") ){
                map.put( "pac.number.rows", value);

            }
        }

        return map;

    }

    /**
     * Performs data validation of the current log's store data
     * */
    private void validateStoreData( String logValue ){
        String logPercentageFree;
        String confValue;
        String confStoreName;
        String configStoreWarning;
        Map<String, String> tempMap;
        boolean matchedStoreName = false;
        Utilities utils = new Utilities(); // TODO: declare as private class variable

        /**
         * Extract the log values from the current log line.
         * */
        String logStoreName = utils.textExtraction( "(?<=Store name: ')(.*)(?=' Store Size:)", logValue );
        String logStoreMaxSize = utils.textExtraction( "(?<=Store Max-Size: ')(.*)(?=' Store Max-Size-Bytes:)", logValue );
        String logStoreFreePercentage = utils.textExtraction( "(?<=Store Free-Percentage: ')(.*)(?=' Store Free-DB-Percentage:)", logValue );
        String logStoreFreeDBPercentage = utils.textExtraction( "(?<=Store Free-DB-Percentage: ')(.*)(?=')", logValue );

        /**
         * Extract the store config data for the current uuid.
         * */
        tempMap = extractConfigurationData("storeConfigData" ); // TODO: this is being called multiple times when it does not need to be. Refactor.

        /**
         * Iterate the configuration data for the current log file
         * */
        for ( Map.Entry<String, String> cData : tempMap.entrySet() ) {
            confValue = cData.getValue();
            confStoreName = utils.textExtraction( "(?<=)(.*)(?=:)", confValue );
            configStoreWarning = utils.textExtraction( "(?<=:)(.*)(?=)", confValue );

            /**
             * Add the log store names to a global list, so they can be compared to the config store names
             * */
            if( _storeNamesFromLog == null ){
                _storeNamesFromLog = new ArrayList<>();
                _storeNamesFromLog.add( logStoreName );
            }else if( !_storeNamesFromLog.contains( logStoreName ) ){
                _storeNamesFromLog.add( logStoreName );
            }

            if( _storeNamesFromConfig == null ){
                _storeNamesFromConfig = new ArrayList<>();
                _storeNamesFromConfig.add( logStoreName );
            }else if( !_storeNamesFromConfig.contains( logStoreName ) ){
                _storeNamesFromConfig.add( logStoreName );
            }

            /**
             * Perform the store.ceiling.warning validation
             * */
            if( logStoreName.equals( confStoreName ) ){
                matchedStoreName = true;

                if( !logStoreMaxSize.contains( "infinity bytes" ) ){
                    if( logStoreFreeDBPercentage == null ){
                        logPercentageFree = logStoreFreePercentage;

                    }
                    else {
                        logPercentageFree = logStoreFreeDBPercentage;

                    }

                    boolean res = checkLogForErrors(
                            logPercentageFree,
                            configStoreWarning,
                            "<" );

                    if( res == false ){
                        _LOGGER.warning("Server name: " + _serverName +
                                ". Store name (from log): " + logStoreName +
                                ". Percent free (from log): " + logPercentageFree +
                                ". store.ceiling.warning: " + configStoreWarning );

                        _summaryData.put( _serverName + "##" + logStoreName + "_store.ceiling.warning",
                                "Configuration Name: store.ceiling.warning Configuration Value: " + configStoreWarning +
                                " Log Value: " + logPercentageFree +
                                " Store Name: " + logStoreName );

                    }
                }
                break;
            }
        }

        /**
         * The log store name was not found in the configuration
         * */
        if( !matchedStoreName ) {
            _LOGGER.warning("Server name: " + _serverName +
                    ". Store name (from log): " + logStoreName +
                    ". log.store.name.not.found.in.config: " );

            _summaryData.put( _serverName + "##" + logStoreName + "_log.store.name.not.found.in.config",
                    "Configuration Name: log.store.name.not.found.in.config Configuration Value: " +
                    " Log Value: " + logStoreName );

        }
    }

    /**
     * Performs data validation of the current log's network data
     * */
    private void validateNetworkData( String logValue ){
        String confValue;
        String configPortNumber;
        String configType;
        String configSSLEnabled;
        String configServiceTrusted;
        String configPeakWarning;
        String logPortNumber = null;
        String logType;
        String logSSLEnabled;
        String logServiceTrusted;
        String logPeak;
        //String logServiceStart;
        //String logServicePeakAt;
        boolean networkFound = false;
        boolean testResult;
        /**
         * Extract the network config data for the current uuid.
         * */
        Map<String, String> tempMap;
        tempMap = extractConfigurationData("networkConfigData" );
        Utilities utils = new Utilities();

        for ( Map.Entry<String, String> cData : tempMap.entrySet() ) {
            confValue = cData.getValue();

            /**
             * Extract the configuration values
             * */
            configPortNumber = utils.textExtraction( "(?<=port:)(.*)(?=##type)", confValue );
            configType = utils.textExtraction( "(?<=type:)(.*)(?=##sslEnabled)", confValue );
            configSSLEnabled = utils.textExtraction( "(?<=sslEnabled:)(.*)(?=##serviceTrusted)", confValue );
            configServiceTrusted = utils.textExtraction( "(?<=serviceTrusted:)(.*)(?=##peakWarning)", confValue );
            configPeakWarning = utils.textExtraction( "(?<=peakWarning:)(.*)(?=)", confValue );
            logPortNumber = utils.textExtraction( "(?<=Service port: ')(.*)(?=' Service type:)", logValue );
            logType = utils.textExtraction( "(?<=Service type: ')(.*)(?=' Service ssl:)", logValue );
            logSSLEnabled = utils.textExtraction( "(?<=Service ssl: ')(.*)(?=' Service trust:)", logValue  );
            logServiceTrusted = utils.textExtraction( "(?<=Service trust: )(.*)(?= Service start:)", logValue  );
            logPeak = utils.textExtraction( "(?<=Service peak: ')(.*)(?=' Service peak-at:)", logValue );

            /**
             * Add the log network to a global list, so they can be compared to the config network ports
             * */

            /**
             * Add the current log store name to global array list
             * */
            if( _networkPortNumberFromLog == null ){
                _networkPortNumberFromLog = new ArrayList<>();
                _networkPortNumberFromLog.add( logPortNumber );
            }else if( !_networkPortNumberFromLog.contains( logPortNumber ) ){
                _networkPortNumberFromLog.add( logPortNumber );
            }

            /**
             * Add the current config store name to global array list
             * */
            if( _networkPortNumberConfig == null ){
                _networkPortNumberConfig = new ArrayList<>();
                _networkPortNumberConfig.add( logPortNumber );
            }else if( !_networkPortNumberConfig.contains( logPortNumber ) ){
                _networkPortNumberConfig.add( logPortNumber );
            }

            if( logPortNumber.equals( configPortNumber ) ){
                networkFound = true;

                /**
                 * Validate network type
                 * */
                testResult = checkLogForErrors(
                        logType,
                        configType,
                        "" );

                if( testResult == false ){
                    _LOGGER.warning(_serverName + ". Port: " + logPortNumber + ". Log network type: " + logType + ". Config network type: " + configType );

                    _summaryData.put( _serverName + "##" + logPortNumber + "_network.type",
                            "Configuration Name: network.type Configuration Value: " + configType + " Log Value: " + logType );

                }

                /**
                 * Validate network ssl status
                 * */
                testResult = checkLogForErrors(
                        logSSLEnabled,
                        configSSLEnabled,
                        "" );

                if( testResult == false ){
                    _LOGGER.warning(_serverName + ". Port: " + logPortNumber +
                            ". Log network ssl status: " + logSSLEnabled +
                            ". Config network ssl status: " + configSSLEnabled );

                    _summaryData.put( _serverName + "##" + logPortNumber + "_network.ssl.enabled",
                            "Configuration Name: network.ssl.enabled Configuration Value: " + configSSLEnabled + " Log Value: " + logSSLEnabled );

                }

                /**
                 * Validate network trust status
                 * */
                testResult = checkLogForErrors(
                        logServiceTrusted,
                        configServiceTrusted,
                        "" );

                if( testResult == false ){
                    _LOGGER.warning(_serverName + ". Port: " + logPortNumber +
                            ". Log network trust status: " + logServiceTrusted +
                            ". Config network trust status: " + configServiceTrusted );

                    _summaryData.put( _serverName + "##" + logPortNumber + "_network.trusted",
                            "Configuration Name: network.trusted Configuration Value: " + configServiceTrusted + " Log Value: " + logServiceTrusted );

                }

                /**
                 * Validate network peak
                 * */
                testResult = checkLogForErrors(
                        logPeak,
                        configPeakWarning,
                        ">" );

                if( testResult == false ){
                    _LOGGER.warning(_serverName + ". Port: " + logPortNumber +
                            ". Log network peak status: " + logPeak +
                            ". Config network peak status: " + configPeakWarning );

                    _summaryData.put( _serverName + "##" + logPortNumber + "_network.peak.warning",
                            "Configuration Name: network.peak.warning Configuration Value: " + configPeakWarning + " Log Value: " + logPeak );

                }
            }
        }

        /**
         * The log network port number was not found in the configuration
         * */
        if( !networkFound ) {
            _LOGGER.warning(_serverName + ". Port: " + logPortNumber +
                    ". Log port number not found in configuration" );

            _summaryData.put( _serverName + "##" + logPortNumber + "_log.network.port.not.found.in.config",
                    "Configuration Name: log.network.port.not.found.in.config Configuration Value: " +
                            " Log Value: " + logPortNumber );

        }
    }

    /**
     * Performs data validation of the current log's job data
     * */
    private void validateJobData( String logValue ){
        boolean matchedJobName = false;
        String logJobName;
        Map<String, String> tempMap;
        String jobName;
        Utilities utils = new Utilities();
        /*String logJobLastExec;
        String logJobNextExec;
        String logJobExecPeriod;
        String logJobExecWhen;*/

        /**
         * Extract the job config data for the current uuid.
         * */
        tempMap = extractConfigurationData("jobConfigData" );

        /**
         * Extract the log values from the current log line.
         * */
        //logJobName = Utilities.extractText("(?<=Job name: ')(.*)(?=' Job last execution:)", logValue );
        logJobName = utils.textExtraction( "(?<=Job name: ')(.*)(?=' Job last execution:)", logValue );
        /*logJobLastExec = Utilities.extractText("(?<=Job last execution: ')(.*)(?=' Job next execution:)", logValue );
        logJobNextExec = Utilities.extractText("(?<=Job next execution: ')(.*)(?=' Job execution period:)", logValue );
        logJobExecPeriod = Utilities.extractText("(?<=Job execution period:)(.*)(?=. Job execution when:)", logValue );
        logJobExecWhen = Utilities.extractText("(?<=Job execution when: ')(.*)(?=')", logValue );*/

        for ( Map.Entry<String, String> cData : tempMap.entrySet() ) {
            jobName = cData.getValue();

            /**
             * Add the log job to a global list, so they can be compared to the config job names
             * */
            /**
             * Add the current log job name to global array list
             * */
            if( _jobNamesFromLog == null ){
                _jobNamesFromLog = new ArrayList<>();
                _jobNamesFromLog.add( logJobName );
            }else if( !_jobNamesFromLog.contains( logJobName ) ){
                _jobNamesFromLog.add( logJobName );
            }

            /**
             * Add the current config job name to global array list
             * */
            if( _jobNamesFromConfig == null ){
                _jobNamesFromConfig = new ArrayList<>();
                _jobNamesFromConfig.add( logJobName );
            }else if( !_jobNamesFromConfig.contains( logJobName ) ){
                _jobNamesFromConfig.add( logJobName );
            }

            if( logJobName.equals( jobName ) ){
                matchedJobName = true;
            }
        }

        /**
         * The log store name was not found in the configuration
         * */
        if( !matchedJobName ) {
            _LOGGER.warning(_serverName + ". Scheduled job not found in configuration. Job name: " + logJobName );

            _summaryData.put( _serverName + "##" + logJobName + "_log.job.name.not.found.in.config",
                    "Configuration Name: log.job.name.not.found.in.config Configuration Value: " +
                            " Log Value: " + logJobName );
        }
    }

    /**
     * Performs data validation of the current log's package data
     * */
    private void validatePackageData( String logValue ){
        boolean matchedPackageName = false;
        String logPackageName;
        String logPackageVersion;
        Map<String, String> tempMap;
        String packageName;
        String packageVersion;
        String configData;

        /**
         * Extract the job config data for the current uuid.
         * */
        tempMap = extractConfigurationData("packageConfigData" );

        /**
         * Extract the log values from the current log line.
         * */
        logPackageName = _utils.textExtraction( "(?<=Package name: ')(.*)(?=' Package version:)", logValue );
        logPackageVersion = _utils.textExtraction( "(?<=Package version: ')(.*)(?=')", logValue );

        for ( Map.Entry<String, String> cData : tempMap.entrySet() ) {
            configData = cData.getValue();
            packageName =configData.split(":")[0];
            packageVersion =configData.split(":")[1];

            /**
             * Add the log package to a global list, so they can be compared to the config package names
             * */
            if( _packageNamesFromLog == null ){
                _packageNamesFromLog = new ArrayList<>();
                _packageNamesFromLog.add( logPackageName + ":" + logPackageVersion );
            }
            else if( !_packageNamesFromLog.contains( logPackageName ) ){
                _packageNamesFromLog.add( logPackageName + ":" + logPackageVersion );
            }

            if( _packageNamesFromConfig == null ){
                _packageNamesFromConfig = new ArrayList<>();
                _packageNamesFromConfig.add( packageName + ":" + packageVersion );
            }
            else if( !_packageNamesFromConfig.contains( packageName ) ){
                _packageNamesFromConfig.add( packageName + ":" + packageVersion );
            }

            if( logPackageName.equals( packageName ) ){
                matchedPackageName = true;

                if( !logPackageVersion.equals( packageVersion ) ){
                    // Package version mismatch
                    _LOGGER.warning(_serverName + ". Package version mismatch. Log package Name: " + logPackageName +
                            ". Config package version: " + packageVersion +
                            ". Log package version: " + logPackageVersion);

                    _summaryData.put( _serverName + "##" + packageVersion + "_package.version.mismatch",
                            "Configuration Name: package.version.mismatch Configuration Value: " + packageVersion +
                            " Log Value: " + "Package name: " + logPackageName + "\nPackage version: " + logPackageVersion );
                }
                break;
            }
        }

        /**
         * The log store name was not found in the configuration
         * */
        if( !matchedPackageName ) {
            _LOGGER.warning(_serverName + ". Log package Name not found in config: " + logPackageName +
                    ". Log package version: " + logPackageVersion);

            _summaryData.put( _serverName + "##" + logPackageName + "_log.package.name.not.found.in.config",
                    "Configuration Name: log.package.name.not.found.in.config Configuration Value: " +
                    " Log Value: " + "Package name: " + logPackageName + "\nPackage version: " + logPackageVersion );
        }
    }

    /**
     * Checks for the existence of a log store name, within the list of stores defined in the configuration ( for the current MF server ).
     * Updates _summaryData object if log entries are found that do not exist in a configuration list.
     * Note: The corresponding check, i.e. log store names that do not exist in the configuration, is performed in validateStoreData()
     * */
    private void compareLists(){
        /**
         * Check to see if a given config store name exists in the logs
         * Note: The removeAll() will remove the non-matching configuration store names based on matches to the log store names
         * */
        if( _storeNamesFromConfig.removeAll( _storeNamesFromLog ) ){
            for( int i = 0; i < _storeNamesFromConfig.size(); i++ ){
                /**
                 * We have discovered a store name listed in the configuration, that has not been found in the list of log store names.
                 * */

                String storeNameConfig = _storeNamesFromConfig.get( i );

                _LOGGER.warning(_serverName + ". Configuration asset store name was not found in the log. Store name: " + storeNameConfig );

                _summaryData.put(
                        _serverName + "##" + storeNameConfig + "_config.store.name.not.found.in.log",
                        "Configuration Name: config.store.name.not.found.in.log" +
                        " Configuration Value: " + storeNameConfig +
                        " Log Value: " );

            }
        }

        if( _networkPortNumberConfig.removeAll( _networkPortNumberFromLog ) ){
            for( int i = 0; i < _networkPortNumberConfig.size(); i++ ){
                /**
                 * We have discovered a store name listed in the configuration, that has not been found in the list of log store names.
                 * */

                String portNumberConfig = _networkPortNumberConfig.get( i );

                _LOGGER.warning(_serverName + ". Configuration network was not found in the log. Port number: " + portNumberConfig );

                _summaryData.put( _serverName + "##" + portNumberConfig + "_network.port.number.config.not.found.in.log",
                        "Configuration Name: network.port.number.config.not.found.in.log Configuration Value: " + portNumberConfig + " Log Value: " );

            }
        }

        if( _jobNamesFromConfig.removeAll( _jobNamesFromLog ) ){
            for( int i = 0; i < _jobNamesFromConfig.size(); i++ ){
                /**
                 * We have discovered a job name listed in the configuration, that has not been found in the list of log job names.
                 * */

                String jobName = _jobNamesFromConfig.get( i );

                _LOGGER.warning(_serverName + ". Configuration scheduled job name was not found in the log. Job name: " + jobName );

                _summaryData.put( _serverName + "##" + jobName + "_config.job.name.not.found.in.log",
                        "Configuration Name: config.job.name.not.found.in.log Configuration Value: " + jobName + " Log Value: " );

            }
        }

        if( _packageNamesFromConfig.removeAll( _packageNamesFromLog ) ){
            for( int i = 0; i < _packageNamesFromConfig.size(); i++ ){
                /**
                 * We have discovered a package name listed in the configuration, that has not been found in the list of log package names.
                 * */

                String packageName = _packageNamesFromConfig.get( i ).split(":")[0];
                String packageVersion = _packageNamesFromConfig.get( i ).split(":")[1];

                _LOGGER.warning(_serverName + ". Configuration package name was not found in the log. Package name: " + packageName );

                _summaryData.put( _serverName + "##" + packageName + "_config.package.name.not.found.in.log",
                        "Configuration Name: config.package.name.not.found.in.log Configuration Value: " + packageName + "\nPackage version: " + packageVersion );

            }
        }
    }

    private Table setTableHeadings( String type, Table table ){

        if( type.equals("store")){
            table.get( 0, 0 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 1 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 2 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 0 ).addParagraph().appendText( "Name" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 1 ).addParagraph().appendText( "Size" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 2 ).addParagraph().appendText( "Free" ).applyCharacterFormat( _cfCellHeading );

        }
        else if( type.equals("job")){
            table.get( 0, 0 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 1 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 2 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 3 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 0 ).addParagraph().appendText( "Name" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 1 ).addParagraph().appendText( "Last Run" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 2 ).addParagraph().appendText( "Next Run" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 3 ).addParagraph().appendText( "Interval" ).applyCharacterFormat( _cfCellHeading );

        }
        else if( type.equals("network")){
            table.get( 0, 0 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 1 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 2 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 3 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 4 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 5 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 0 ).addParagraph().appendText( "Port" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 1 ).addParagraph().appendText( "Type" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 2 ).addParagraph().appendText( "SSL Enabled" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 3 ).addParagraph().appendText( "Started" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 4 ).addParagraph().appendText( "Peak" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 5 ).addParagraph().appendText( "Peak-At" ).applyCharacterFormat( _cfCellHeading );

        }
        else if( type.equals("package")){
            table.get( 0, 0 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 1 ).getCellFormat().setBackColor(Color.black);
            table.get( 0, 0 ).addParagraph().appendText( "Package Name" ).applyCharacterFormat( _cfCellHeading );
            table.get( 0, 1 ).addParagraph().appendText( "Package Version" ).applyCharacterFormat( _cfCellHeading );
        }

        return table;

    }
}
