package HealthReportGenerator;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.logging.*;

import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ApplicationLogging {
    private final Logger _logger = Logger.getLogger( ApplicationLogging.class.getName() );
    private final Handler _consoleHandler = new ConsoleHandler();
    private Handler _fileHandler;
    private Level _level;
    private final Map<String, String> _globalConfigData;
    private String _termList = String.valueOf( Arrays.asList( "SEVERE","WARNING", "INFO", "CONFIG", "FINE", "FINER", "FINEST" ) );
    private String _applicationLogFilePath;

    public ApplicationLogging(){
        /**
         * Get the application config settings.
         * */
        _globalConfigData = new ConfigurationData().globalConfigData();

        /**
         * Get the application log level and ensure provided level is acceptable.
         * */
        String applicationLogLevel = _globalConfigData.get( "application.log.level" ).toUpperCase(Locale.ROOT);
        if( applicationLogLevel == null || applicationLogLevel == "" ){
            System.out.println( "Exception. Please provide a valid application.log.level. Provided: " + applicationLogLevel );
            System.exit(-1);
        }
        else if( !_termList.contains( applicationLogLevel ) ){
            System.out.println( "Exception. Please provide a valid application.log.level. Provided: " + applicationLogLevel );
            System.exit(-1);
        }

        if( applicationLogLevel.contains( "SEVERE" ) ){
            _level = Level.SEVERE;
        }
        else if( applicationLogLevel.contains( "WARNING" ) ){
            _level = Level.WARNING;
        }
        else if( applicationLogLevel.contains( "INFO" ) ){
            _level = Level.INFO;
        }
        else if( applicationLogLevel.contains( "CONFIG" ) ){
            _level = Level.CONFIG;
        }
        else if( applicationLogLevel.contains( "FINE" ) ){
            _level = Level.FINE;
        }
        else if( applicationLogLevel.contains( "FINER" ) ){
            _level = Level.FINER;
        }
        else if( applicationLogLevel.contains( "FINEST" ) ){
            _level = Level.FINEST;
        }

        /**
         * Get and check the path to the application log directory.
         * */
        _applicationLogFilePath = _globalConfigData.get( "application.log.directory" );
        if( !Utilities.testDirectoryExists( _applicationLogFilePath ) ){
            System.out.println( "Exception. Please provide a valid application log directory. Provided: " + _applicationLogFilePath );
            System.exit(-1);
        }

        initialiseHealthReportLogger();
    }

    /**
     * Start the logger
     * */
    public void startLogger(){
        setStartLogger();

    }

    /**
     * Sets the logger
     * */
    private void setStartLogger(){
        initialiseHealthReportLogger();

    }

    /**
     * Log a message to the defined log output buffer
     * */
    public void logMessage( String msg ) {
        setLogMessage( msg );

    }

    /**
     * Sets the message to the defined log output buffer
     * */
    private void setLogMessage( String msg ) {
        appLogger( msg );

    }

    private void appLogger( String logMessage ){
        //_logger.info( logMessage );
        _logger.log(Level.INFO, logMessage );
        _logger.log(Level.WARNING, logMessage );
        _logger.log(Level.SEVERE, logMessage );
        //_logger.log( Lev, "test");
        //_logger.info( "info" );
        //_logger.logp(_level, "sourceClassName", "sourceMethodName", "The message.");

        /*Handler consoleHandler;
        Handler fileHandler;
        Logger logger = Logger.getLogger( ApplicationLogging.class.getName() );*/

        try{
            //Creating consoleHandler and fileHandler
            /*consoleHandler = new ConsoleHandler();
            fileHandler  = new FileHandler("./content/ApplicationLog/healthReporter.log");*/

            //Assigning handlers to LOGGER object
            /*logger.addHandler( consoleHandler );
            logger.addHandler( fileHandler );*/

            /*logger.info("Logger Name: "+ logger.getClass().getSimpleName() );
            logger.warning("Can cause BLAH exception");*/

            //Setting levels to handlers and LOGGER
            /*consoleHandler.setLevel( Level.INFO );
            fileHandler.setLevel( Level.INFO );
            logger.setLevel( Level.INFO );
            logger.log( Level.INFO, logMessage );
            logger.config( logMessage );*/

            //Console handler removed
            //logger.removeHandler( consoleHandler );

            //logger.log(Level.FINE, "Finest logging");

        }
        catch (Exception e) {
            //logger.log(Level.SEVERE, "Error occur in FileHandler.", e );
        }

        //logger.finer("Finest example on LOGGER handler completed.");
    }

    private void initialiseHealthReportLogger(){
        try {
            _fileHandler = new FileHandler(_applicationLogFilePath + "healthReporter.log" );
            _fileHandler.setFormatter( new SimpleFormatter() );

            _logger.addHandler( _fileHandler );
            _logger.setLevel( _level );

            LogManager.getLogManager().getLogger( Logger.GLOBAL_LOGGER_NAME ).setLevel( _level );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
