package HealthReportGenerator;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * <h1>The ConfigurationData class is responsible for the construction of configuration data objects.</h1>
 * <p>
 * The ConfigurationData class will perform the construction of the following configuration data sets;
 * <ul style=“list-style-type:square”>
 *     <li>The construction of the Mediaflux Environment Health Reporting Tool's, application configuration data.</li>
 *     <li>The construction of the Mediaflux job, store, network and package configuration data.</li>
 *     <li>The construction of the Mediaflux server properties configuration data.</li>
 * </ul>
 *
 * @author  Jayson Brenton
 */
public class ConfigurationData {
    private Map<String, String> _applicationConfigurationData = new HashMap<>();
    private Map<String, String> _serverJobsConfigurationData = new HashMap<>();
    private Map<String, String> _serverStoresConfigurationData = new HashMap<>();
    private Map<String, String> _serverNetworksConfigurationData = new HashMap<>();
    private Map<String, String> _serverPackagesConfigurationData = new HashMap<>();
    private Map<String, String> _serverPropertiesConfigurationData = new HashMap<>();

    /**
     * use the classname for the logger
     */
    private final static Logger _LOGGER = Logger.getLogger( Logger.GLOBAL_LOGGER_NAME );

    /**
    * Returns the application configuration properties
    * */
    public Map<String, String> globalConfigData() {
        return _applicationConfigurationData;
    }

    /**
     * Sets the application configuration properties
     * */
    private void setGlobalConfigData() {
        _applicationConfigurationData = configurationData("/config/properties/property");
    }

    /**
     * Returns the job's configuration properties for all servers
     * */
    public Map<String, String> jobsConfigData( ) {
        return _serverJobsConfigurationData;

    }

    /**
     * Sets the job's configuration properties for all servers
     * */
    private void setServerJobsConfigData() {
        _serverJobsConfigurationData = configurationData("/config/servers//server/expected-jobs/descendant::*");
    }

    /**
     * Returns the store's configuration properties for all servers
     * */
    public Map<String, String> storesConfigData( ) {
        return _serverStoresConfigurationData;

    }

    /**
     * Sets the store's configuration properties for all servers
     * */
    private void setServerStoresConfigData() {
        _serverStoresConfigurationData = configurationData("/config/servers//server/expected-stores/descendant::*");

    }

    /**
     * Returns the network's configuration properties for all servers
     * */
    public Map<String, String> networksConfigData( ) {
        return _serverNetworksConfigurationData;

    }

    /**
     * Sets the network's configuration properties for all servers
     * */
    private void setServerNetworksConfigData() {
        _serverNetworksConfigurationData = configurationData("/config/servers//server/expected-networks/*");

    }

    /**
     * Returns the configuration packages for all servers
     * */
    public Map<String, String> packagesConfigData( ) {
        return _serverPackagesConfigurationData;

    }

    /**
     * Sets the configuration packages for all servers
     * */
    private void setServerPackagesConfigData() {
        _serverPackagesConfigurationData = configurationData("/config/servers//server/expected-packages/descendant::*");

    }

    /**
     * Returns the configuration properties for all servers
     * */
    public Map<String, String> serverPropertiesConfigData( ) {
        return _serverPropertiesConfigurationData;

    }

    /**
     * Sets the configuration properties for all servers
     * */
    private void setServerPropertiesConfigData() {
        _serverPropertiesConfigurationData = configurationData("/config/servers/server/property");

    }

    /**
     * Constructor
     * */
    public ConfigurationData( ){
        setGlobalConfigData();
        setServerPropertiesConfigData();
        setServerJobsConfigData();
        setServerStoresConfigData();
        setServerNetworksConfigData();
        setServerPackagesConfigData();

    }

    /**
     * Returns the configuration data
     * */
    private Map<String, String> configurationData( String xpathExpression ) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware( true );
        DocumentBuilder builder;
        Document doc;
        XPath xpath;

        /**
         * Parse the config file
         * */
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse("config/HealthReportGenerator.xml" );
            XPathFactory xpathFactory = XPathFactory.newInstance();
            xpath = xpathFactory.newXPath();

        }
        catch (ParserConfigurationException | SAXException | IOException e) {
            return null;

        }

        Map<String, String> propsMap = new HashMap<>();
        Map<String, String> tmpMap;
        int networkCounter = 0;
        int jobsCounter = 0;
        int storesCounter = 0;
        int serverPropertyCounter = 0;
        int globalPropertyCounter = 0;
        int packageCounter = 0;

        /**
         * Iterate the node list in the xml config file
         * Calls setConfigData()
         * */
        try {
            XPathExpression expr = xpath.compile( xpathExpression );
            NodeList nodes = (NodeList) expr.evaluate( doc, XPathConstants.NODESET );

            for (int loopCounter = 0; loopCounter < nodes.getLength(); loopCounter++ ){
                /**
                 * Set the configuration's application properties
                 * */
                if( xpathExpression.contains( "/config/properties/property" ) ){
                    tmpMap = setConfigData( nodes.item( loopCounter ), globalPropertyCounter, "global" );
                    propsMap.putAll( tmpMap );
                    globalPropertyCounter++;

                }
                /**
                 * set the Mediaflux properties configuration for each server defined in the configuration
                 * */
                else if( xpathExpression.contains( "/config/servers/server/property" ) ){
                    tmpMap = setConfigData( nodes.item( loopCounter ), serverPropertyCounter, "serverProperties" );
                    propsMap.putAll( tmpMap );
                    serverPropertyCounter++;

                }
                /**
                 * set the Mediaflux network configuration for each server defined in the configuration
                 * */
                else if(xpathExpression.contains( "/config/servers//server/expected-networks/" ) ){
                    tmpMap = setConfigData( nodes.item( loopCounter ), networkCounter, "networkProperties" );
                    propsMap.putAll( tmpMap );
                    networkCounter++;

                }
                /**
                 * set the Mediaflux jobs configuration for each server defined in the configuration
                 * */
                else if(xpathExpression.contains( "/config/servers//server/expected-jobs/" )){
                    tmpMap = setConfigData( nodes.item( loopCounter ), jobsCounter, "jobProperties" );
                    propsMap.putAll( tmpMap );
                    jobsCounter++;

                }
                /**
                 * set the Mediaflux asset store configuration for each server defined in the configuration
                 * */
                else if(xpathExpression.contains( "/config/servers//server/expected-stores/" ) ){
                    tmpMap = setConfigData( nodes.item( loopCounter ), storesCounter, "storeProperties" );
                    propsMap.putAll( tmpMap );
                    storesCounter++;

                }
                /**
                 * set the Mediaflux installed package configuration for each server defined in the configuration
                 * */
                else if(xpathExpression.contains( "/config/servers//server/expected-packages/" ) ){
                    tmpMap = setConfigData( nodes.item( loopCounter ), packageCounter, "packageProperties" );
                    propsMap.putAll( tmpMap );
                    packageCounter++;

                }
            }

        }
        catch ( XPathExpressionException e ) {
            _LOGGER.severe( e.getMessage() );
            return null;

        }

        return propsMap;
    }

    /**
     * Sets the configuration data
     * */
    private Map<String, String> setConfigData( Node nodes, int counter, String type ){
        Map<String, String> propsMap = new HashMap<>();
        String configItemName;
        String configItemValue;
        String mfUUID;

        /**
         * Creates map containing total configuration data
         * */
        switch (type) {
            case "global":
                configItemName = nodes.getAttributes().item(0).getNodeValue();
                configItemValue = nodes.getFirstChild().getNodeValue();
                propsMap.put(configItemName, configItemValue);

                _LOGGER.finest("Set global config data. Name: " + configItemName + ". Value: " + configItemValue );

                break;
            case "serverProperties":
                // Get the Mediaflux server name from the current node
                mfUUID = nodes.getParentNode().getAttributes().item(1).getNodeValue().trim();

                // Set the map key
                configItemName = mfUUID + "_property_" + counter;

                // Set the map value
                configItemValue = nodes.getAttributes().item(0).getNodeValue() + ":" + nodes.getFirstChild().getNodeValue();

                propsMap.put(configItemName, configItemValue);

                _LOGGER.finest("Set server property config data. Name: " + configItemName + ". Value: " + configItemValue );

                break;
            case "networkProperties":
                // Get the Mediaflux uuid from the current node
                mfUUID = nodes.getParentNode().getParentNode().getAttributes().item(1).getNodeValue();

                // Get the Mediaflux network port number from the current node
                String portNum = nodes.getChildNodes().item(1).getChildNodes().item(0).getNodeValue();

                // Get the Mediaflux network type from the current node
                String netType = nodes.getChildNodes().item(3).getChildNodes().item(0).getNodeValue();

                // Get the Mediaflux network ssl status from the current node
                String sslEnabled = nodes.getChildNodes().item(5).getChildNodes().item(0).getNodeValue();

                // Get the Mediaflux network trust status from the current node
                String serviceTrust = nodes.getChildNodes().item(7).getChildNodes().item(0).getNodeValue();

                // Get the Mediaflux network peak warning number from the current node
                String peakWarning = nodes.getChildNodes().item(9).getChildNodes().item(0).getNodeValue();

                // Set the map key
                configItemName = mfUUID + "_" + "network" + "_" + counter;

                // Set the map value
                configItemValue = "port:" + portNum + "##type:" + netType + "##sslEnabled:" + sslEnabled + "##serviceTrusted:" + serviceTrust + "##peakWarning:" + peakWarning;

                // Populate the map with the current network data
                propsMap.put(configItemName, configItemValue);

                _LOGGER.finest("Set network config data. Name: " + configItemName + ". Value: " + configItemValue );

                break;
            case "jobProperties":
                // Get the Mediaflux server name from the current node
                mfUUID = nodes.getParentNode().getParentNode().getAttributes().item(1).getNodeValue();

                // Set the map value
                configItemValue = nodes.getFirstChild().getNodeValue();

                // Set the map key
                configItemName = mfUUID + "_job_" + counter;

                // Populate the map with the current network data
                propsMap.put(configItemName, configItemValue);

                _LOGGER.finest("Set job config data. Name: " + configItemName + ". Value: " + configItemValue );

                break;
            case "storeProperties":
                // Get the Mediaflux server name from the current node
                mfUUID = nodes.getParentNode().getParentNode().getAttributes().item(1).getNodeValue();

                // Set the map value
                configItemValue = nodes.getFirstChild().getNodeValue() + ":" + nodes.getAttributes().item(0).getNodeValue();

                // Set the map key
                configItemName = mfUUID + "_store_" + counter;

                // Populate the map with the current network data
                propsMap.put(configItemName, configItemValue);

                _LOGGER.finest("Set store config data. Name: " + configItemName + ". Value: " + configItemValue );

                break;
            case "packageProperties":
                // Get the Mediaflux server name from the current node
                mfUUID = nodes.getParentNode().getParentNode().getAttributes().item(1).getNodeValue();

                // Set the map value
                configItemValue = nodes.getFirstChild().getNodeValue() + ":" + nodes.getAttributes().item(0).getNodeValue();

                // Set the map key
                configItemName = mfUUID + "_package_" + counter;

                // Populate the map with the current network data
                propsMap.put(configItemName, configItemValue);

                _LOGGER.finest("Set package config data. Name: " + configItemName + ". Value: " + configItemValue );

                break;
        }

        return propsMap;
    }
}
