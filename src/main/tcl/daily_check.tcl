#================================================================================
# Script to output key information about the server during daily reviews
#
#     09-Feb-2022   Jayson Brenton, Arcitecta
#
#================================================================================

# ------------------------------------------------------------------------
# This script will;
#   : Determine the server type ( primary/secondary ) by getting "/server/name" from server.identity then extracting either primary or secondary from the server name.
#   : Capture the current server version, current time, last server restart time, server uptime, assigned max memory & average server load using server.status.
#   : Determine if the replication has been running on the primary server using xodb.status.
#   : Store statistics, store name, current store size & store free space using asset.store.describe.
#   : Scheduled job statistics, job name, last & next execution time.
# ------------------------------------------------------------------------

set LOG_NAME "health-check"
set serverType ""

proc logDebug { message } {
    global LOG_NAME
    server.log :app $LOG_NAME :event debug :filter ignore :msg "$message"
    puts $message

}

# Log the server name.
# Determine if primary or secondary.
# If secondary, we won't run the xodb.status to check the replication statistics.
# ================================================================================
set serverDetails [ server.identity ]
set serverName [ xvalue "/server/name" $serverDetails ]

logDebug "Heading: 'Executing Daily Health Check Process'"
logDebug "Parameter: 'Mediaflux Server Name' Value: '${serverName}'"

if { [ string match "*primary*" [string tolower $serverName] ] == 1 } {
    set serverType "primary"
    logDebug "Parameter: 'Server type' Value: 'primary'"

} elseif { [ string match "*secondary*" [string tolower $serverName] ] == 1 } {
    set serverType "secondary"
    logDebug "Parameter: 'Server type' Value: 'secondary'"

} else {
    set serverType "unknown"
    logDebug "Parameter: 'Server type' Value: 'unknown'"

}

# Capture installed package details
set result [ package.list ]
set continue "true"
set i "0"

logDebug "Heading: 'Mediaflux Package Details'"
while { ${continue} == "true" } {
	#set name [xvalue job\[${i}\]/@name $results]
    set packageName [ xvalue "/package\[${i}\]" $result ]
    set packageVersion [ xvalue "/package\[${i}\]/@version" $result ]

    if { ${packageVersion} == "" } {
        set continue "false"

    } else {
        logDebug "Package name: '${packageName}' Package version: '${packageVersion}'"
        incr i

    }
}

# Capture certificate details
#set result [ server.certificate.identity.describe ]
#set certIsValid [ xvalue "/identity/valid" $result ]
#set certValidToDate [ xvalue "/identity/certificate/validity/to" $result ]
#set certValidInDays [ xvalue "/identity/certificate/validity/valid/@remaining" $result ]

#logDebug "Heading: 'Mediaflux Certificate Details'"
#logDebug "Parameter: 'Is Certificate Valid' Value: '${certIsValid}'"
#logDebug "Parameter: 'Certificate Valid to Date' Value: '${certValidToDate}'"
#logDebug "Parameter: 'Certificate Valid in Days' Value: '${certValidInDays}'"

# Capture server.version build time
set result [ server.version ]
set buildTime [ xvalue "/build-time" $result ]

logDebug "Heading: 'Mediaflux Build Time'"
logDebug "Parameter: 'MF Build Time' Value: '${buildTime}'"

# Capture licence.describe.
set result [ licence.describe ]
set uuid [ xvalue "/uuid" $result ]
set licExp [ xvalue "/licence/expiry" $result ]

logDebug "Heading: 'Mediaflux Licence Details'"
logDebug "Parameter: 'MF uuid' Value: '${uuid}'"
logDebug "Parameter: 'MF License Expiry Date' Value: '${licExp}'"

# Capture server.status.
set result [ server.status ]
set serverVersion [ xvalue "/version" $result ]
set startTime [ xvalue "/start-time" $result ]
set currentTime [ xvalue "/current-time" $result ]
set upTime [ xvalue "/uptime" $result ]
set numCPU [ xvalue "/cpus" $result ]
set avgLoad [ xvalue "/operating-system/system-load-average" $result ]
set mfOSarchitecture [ xvalue "/operating-system/architecture" $result ]
set mfOSVersion [ xvalue "/operating-system/version" $result ]
set mfOSName [ xvalue "/operating-system/name" $result ]
set nbopen [ xvalue "/streams/nbopen" $result ]
set memFreeBytes [ xvalue "/memory/free/@bytes" $result ]
set memTotalBytes [ xvalue "/memory/total/@bytes" $result ]
set memUsedBytes [ xvalue "/memory/used/@bytes" $result ]
set memMaxBytes [ xvalue "/memory/max/@bytes" $result ]
set memMax [ xvalue "/memory/max" $result ]
set memUsed [ xvalue "/memory/used" $result ]
set memFree [ xvalue "/memory/free" $result ]

logDebug "Heading: 'Mediaflux Server Status'"
logDebug "Parameter: 'Version' Value: '${serverVersion}'"
logDebug "Parameter: 'Start Time' Value: '${startTime}'"
logDebug "Parameter: 'Current Time' Value: '${currentTime}'"
logDebug "Parameter: 'Up Time' Value: '${upTime}'"
logDebug "Parameter: 'Assigned Memory' Value: '${memMax}'"
logDebug "Parameter: 'Used Memory' Value: '${memUsed}'"
logDebug "Parameter: 'Free Memory' Value: '${memFree}'"
logDebug "Parameter: 'Average Load' Value: '${avgLoad}'"
logDebug "Parameter: 'Mediaflux OS Architecture' Value: '${mfOSarchitecture}'"
logDebug "Parameter: 'Mediaflux OS Version' Value: '${mfOSVersion}'"
logDebug "Parameter: 'Mediaflux OS Name' Value: '${mfOSName}'"
logDebug "Parameter: 'Open Streams' Value: '${nbopen}'"
logDebug "Parameter: 'Num CPUs' Value: '${numCPU}'"
logDebug "Parameter: 'Free Memory Bytes' Value: '${memFreeBytes}'"
logDebug "Parameter: 'Total Memory Bytes' Value: '${memTotalBytes}'"
logDebug "Parameter: 'Used Memory Bytes' Value: '${memUsedBytes}'"
logDebug "Parameter: 'Max Memory Bytes' Value: '${memMaxBytes}'"

set results [ asset.count ]
set totalNumAssets [ xvalue "//total" $result ]
logDebug "Parameter: 'Number of Assets' Value: '${totalNumAssets}'"

# Capture xodb.status stats.
if { ${serverType} == "primary"  } {
    set result [ xodb.status ]
    set repStartTime [ xvalue "/replicating-database/replicator/last-send/start-time" $result ]
    set repEndTime [ xvalue "/replicating-database/replicator/last-send/end-time" $result ]
    set repDuration [ xvalue "/replicating-database/replicator/last-send/duration" $result ]
    set repDurationUnits [ xvalue "/replicating-database/replicator/last-send/duration" $result ]
    set repFailed [ xvalue "replicating-database/replicator/queue/replicate/failed" $result ]

    logDebug "Heading: 'Mediaflux Server XODB Status'"
    logDebug "Parameter: 'Replication Start Time' Value: '${repStartTime}'"
    logDebug "Parameter: 'Replication End Time' Value: '${repEndTime}'"
    logDebug "Parameter: 'Replication Duration' Value: '${repDuration}'"
    logDebug "Parameter: 'Replication Failed' Value: '${repFailed}'"
}

logDebug "Heading: 'Mediaflux Store Status'"
set results [ asset.store.describe ]
set continue "true"
set i "0"

while { ${continue} == "true" } {
    set name [xvalue store\[${i}\]/@name $results]
    set maxSize [xvalue store\[${i}\]/mount/max-size/@h $results]
    set maxSizeBytes [xvalue store\[${i}\]/mount/max-size $results]
    set freePercentage [xvalue store\[${i}\]/mount/free/@partition-percent $results]
    set freeDBPercentage [xvalue store\[${i}\]/mount/free/@percent $results]
    set size [xvalue store\[${i}\]/mount/size/@h $results]
    set free [xvalue store\[${i}\]/mount/free/@h $results]

    if { ${name} == "" } {
        set continue "false"

    } else {
        logDebug "Store name: '${name}' Store Size: '${size}' Store Free: '${free}' Store Max-Size: '${maxSize}' Store Max-Size-Bytes: '${maxSizeBytes}' Store Free-Percentage: '${freePercentage}'  Store Free-DB-Percentage: '${freeDBPercentage}'"
        incr i

    }
}

logDebug "Heading: 'Mediaflux Scheduled Job Status'"
set results [ schedule.job.describe ]
set continue "true"
set i "0"

while { ${continue} == "true" } {
    set name [xvalue job\[${i}\]/@name $results]
    set last [xvalue job\[${i}\]/last $results]
    set next [xvalue job\[${i}\]/next $results]
    set countHours [ xcount job\[${i}\]/when/hour $results ]
    set countMinutes [ xcount job\[${i}\]/when/minute $results ]
    set period ""

    if { ${name} == "" } {
        set continue "false"

    } else {
        set counter "0"

    if { ${countMinutes} > 0 } {
        set counter $countMinutes
        set period "minute"

    }

    if { ${countHours} > 0 } {
        set counter $countHours
        set period "hour"

    }

    if { ${counter} > 0 } {
        set incrementor 0
        set cont "true"
        set res ""

        # Retrieve the minutes / hours from the scheduled job.
        while { ${cont} == "true" } {
            if { $incrementor == $counter } {
                set cont "false"

            } else {
                set txt ", "
                append res [ xvalue job\[${i}\]/when/${period}\[${incrementor}\] $results ]$txt
                incr incrementor

            }
        }

        set when [string trim $res ", "]
        if { [ string length ${when} ] > 15 } {
            set start [ string range ${when} 0 1 ]
            set start [string trim $start ", "]
            set whenLen [ string length ${when} ]
            set end [ string range ${when} [ expr [ string length ${when} ] -2 ] ${whenLen} ]
            set when "${start} - ${end}"

        }

        logDebug "Job name: '${name}' Job last execution: '${last}' Job next execution: '${next}' Job execution period: ${period} Job execution when: '${when}'"

    } else {
        logDebug "Parameter: 'Counter failed to set' Value: 'Error: Failed to determine interval for job: '${name}'"

    }
    # increment i
    incr i

    }
}

logDebug "Heading: 'Mediaflux Network Status'"
set results [ network.describe ]
set continue "true"
set i "0"

while { ${continue} == "true" } {
	set svcType [xvalue service\[${i}\]/@type $results]
	set svcPort [xvalue service\[${i}\]/@port $results]
	set svcSSL [xvalue service\[${i}\]/@ssl $results]
	set svcTrusted [xvalue service\[${i}\]/@trusted $results]

	set svcStartTime [xvalue service\[${i}\]/stime $results]
	#set svcActive [xvalue service\[${i}\]/connections/active $results]
	set svcPeak [xvalue service\[${i}\]/connections/active/@peak $results]
	set svcPeakAt [xvalue service\[${i}\]/connections/active/@peak-at $results]

  if { ${svcType} == "" } {
	  set continue "false"

	  } else {
	    if { ${svcType} == "smb" } {
	    }
        logDebug "Service port: '${svcPort}' Service type: '${svcType}' Service ssl: '${svcSSL}' Service trust: ${svcTrusted} Service start: '${svcStartTime}' Service peak: '${svcPeak}' Service peak-at: '${svcPeakAt}'"
	    incr i

		}
	}
# server.log.get :name health-check :out !auto